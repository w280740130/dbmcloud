<?php
namespace api\models;

use backend\models\CrmCustomer;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class BindForm extends \yii\db\ActiveRecord
{
    public $nickname;
    public $openid;


    public static function tableName(){
         return 'crm_customer_bind';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['mobile', 'filter', 'filter' => 'trim'],
            ['mobile', 'required'],
            [['mobile'], 'number'],
            [['mobile'], 'string', 'min'=>11,'max' => 11],

            ['identity_card', 'required'],
            [['identity_card'], 'string', 'min'=>18,'max' => 18],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        return array_merge(
            $labels,
            [
                'mobile' => '手机号码',
                'identity_card' => '身份证号',
                'nickname' => '微信名称',

            ]
        );
    }


    public static function check_bind($openid){
        $result = BindForm::find()->where(['wechat_openid'=>$openid])->count();
        return $result;
    }

    public static function check_customer($identity_card,$mobile){
        $result = CrmCustomer::find()->where(['identity_card'=>$identity_card,'mobile'=>$mobile])->count();
        //echo $result;exit;
        return $result;
    }
}
