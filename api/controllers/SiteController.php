<?php
namespace api\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use api\models\SignupForm;
use api\models\CrmUser;
use api\models\BindForm;

/**
 * Site controller
 */
class SiteController extends Controller
{


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSignup()
    {
        $model = new CrmUser();
        $model->loadDefaultValues();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $post_data = Yii::$app->getRequest()->post();
            $model->realname = $post_data['CrmUser']['realname'];
            $model->mobile = $post_data['CrmUser']['mobile'];
            $model->password = $post_data['CrmUser']['password'];
            $model->created_at = time();
            $model->save();
            return $this->redirect('success');
            //print_r($model->errors);
        }

        $this->layout = '@app/views/layouts/main2.php';
        $title = '注册账号';
        return $this->render('signup', [
            'model' => $model,
            'title' => $title,
        ]);
    }

    public function actionSuccess()
    {
        $this->layout = '@app/views/layouts/main2.php';
        $title = '操作成功';
        $msg = '注册成功，稍后会有客服人员与您联系';
        return $this->render('success', ['title' => $title, 'msg' => $msg]);
    }

    public function actionBind()
    {

        $model = new BindForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $post_data = Yii::$app->request->post();
            $openid = $post_data['BindForm']['openid'];

            if ($model->check_customer($post_data['BindForm']['identity_card'],$post_data['BindForm']['mobile']) < 1) {
                throw new ForbiddenHttpException(Yii::t('app', 'No Customer Found'));
            }

            if ($model->check_bind($openid) > 0) {
                throw new ForbiddenHttpException(Yii::t('app', 'Already Bind'));
            }
            //print_r($post_data);exit;
            $model->identity_card = $post_data['BindForm']['identity_card'];
            $model->mobile = $post_data['BindForm']['mobile'];
            $model->wechat_openid = $post_data['BindForm']['openid'];
            $model->wechat_nickname = $post_data['BindForm']['nickname'];
            $model->save();

            return $this->redirect('success-bind');

        }

        $openid = isset($_GET['openid']) ? $_GET['openid'] : '123456';
        $nickname = isset($_GET['nickname']) ? $_GET['nickname'] : 'jz-jc';
        if (empty($openid) || empty($nickname)) {
            //throw new NotFoundHttpException(Yii::t('app', 'Not Found'));
        }

        $model->nickname = $nickname;
        $model->openid = $openid;
        $this->layout = '@app/views/layouts/main2.php';
        return $this->render('bind', [
            'model' => $model,
        ]);
    }

    public function actionSuccessBind()
    {
        $this->layout = '@app/views/layouts/main2.php';
        $title = '操作成功';
        $msg = '绑定成功，稍后您可以使用微信查询您的收款信息';
        return $this->render('success', ['title' => $title, 'msg' => $msg]);
    }


}
