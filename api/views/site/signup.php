<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \api\models\SignupForm */

$this->title = '注册账号';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <p>注册后后我们客服人员会与您取得联系。</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'mobile') ?>
                <?= $form->field($model, 'password') ?>
                <?= $form->field($model, 'realname') ?>
                <div class="form-group">
                    <?= Html::submitButton('提交注册', ['class' => 'btn btn-success btn-large', 'name' => 'signup-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
