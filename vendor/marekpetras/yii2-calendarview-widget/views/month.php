<table class="month <?=$monthRendered?>">
<thead>
    <th colspan="8">
        <div class="btn-group btn-group-justified">
                <a href="" class="navigate btn btn-primary" data-show="<?=$monthRendered-1?>" data-hide="<?=$monthRendered?>">&#8678; 向前</a>
                <a href="" class="navigate btn btn-primary disabled hidden-xs hidden-sm"><?=$title?></a>
                <a href="" class="navigate btn btn-primary disabled hidden-md hidden-lg"><?=$title?></a>
                <a href="" class="navigate btn btn-primary" data-show="<?=$monthRendered+1?>" data-hide="<?=$monthRendered?>">向后 &#8680;</a>
        </div>
    </th>
</thead>
<thead>
<th class="hidden-xs hidden-sm"></th>
<th class="hidden-xs hidden-sm">周日</th>
<th class="hidden-xs hidden-sm">周一</th>
<th class="hidden-xs hidden-sm">周二</th>
<th class="hidden-xs hidden-sm">周三</th>
<th class="hidden-xs hidden-sm">周四</th>
<th class="hidden-xs hidden-sm">周五</th>
<th class="hidden-xs hidden-sm">周六</th>
<th class="hidden-md hidden-lg">W</th>
<th class="hidden-md hidden-lg">M</th>
<th class="hidden-md hidden-lg">T</th>
<th class="hidden-md hidden-lg">W</th>
<th class="hidden-md hidden-lg">T</th>
<th class="hidden-md hidden-lg">F</th>
<th class="hidden-md hidden-lg">S</th>
<th class="hidden-md hidden-lg">S</th>
</thead>

<?=$content?>

</table>