<?php
/**
 * Created by PhpStorm.
 * User: Алимжан
 * Date: 02.02.2015
 * Time: 12:22
 */

return [
    'The model cannot be empty.' => '当前模型不能为空',
    'The behavior FileBehavior has not been attached to the model.' => 'Поведение FileBehavior не привязано к модели.',
    'File name' => '文件名'
];