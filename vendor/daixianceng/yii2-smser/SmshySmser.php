<?php

namespace daixianceng\smser;

use yii\base\NotSupportedException;

/**
 * 海岩短信
 * http://www.smshy.cn/
 * @author Cosmo <daixianceng@gmail.com>
 * @property string $password write-only password
 * @property string $state read-only state
 * @property string $message read-only message
 */
class SmshySmser extends Smser
{
    public $userid;

    public $account;

    public $password;
    /**
     * @inheritdoc
     */
    public $url = 'http://www.duanxin10086.com/sms.aspx';
    
    /**
     * @inheritdoc
     */
    public function send($mobile, $content)
    {
        if (parent::send($mobile, $content)) {
            return true;
        }
        
        $data = [
            'action' => 'send',
            'userid' => $this->userid,
            'account' => $this->account,
            'password' => $this->password,
            'mobile' => $mobile,
            'content' => $content
        ];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        
        $this->state = (string) curl_exec($ch);
        curl_close($ch);
        
        $success = false;
        $result_array = $this->xml_to_array($this->state);
        $state = $result_array['returnsms']['returnstatus'];
        if ($state=='Success') {
            $success = true;
            $this->message = '短信发送成功';
        }else{
            $success = false;
            $this->message = '短信发送失败';
        }
        return $success;
    }
    
    /**
     * @inheritdoc
     */
    public function sendByTemplate($mobile, $data, $id)
    {
        throw new NotSupportedException('商信通不支持发送模板短信！');
    }

    // Xml 转 数组, 包括根键
    public static function xml_to_array($xml)
    {
        $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
        if (preg_match_all($reg, $xml, $matches)) {
            $count = count($matches[0]);
            for ($i = 0; $i < $count; $i++) {
                $subxml = $matches[2][$i];
                $key = $matches[1][$i];
                if (preg_match($reg, $subxml)) {
                    $arr[$key] = self::xml_to_array($subxml);
                } else {
                    $arr[$key] = $subxml;
                }
            }
        }
        return $arr;
    }
}