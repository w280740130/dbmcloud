#!/bin/env python
#-*-coding:utf-8-*-

import string
import sys 
reload(sys) 
sys.setdefaultencoding('utf8')
import ConfigParser
import smtplib
from email.message import Message
from email.header import Header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import functions as func

def get_config2(group,config_name):
    config = ConfigParser.ConfigParser()
    config.readfp(open('./etc/config.ini','rw'))
    config_value=config.get(group,config_name).strip(' ').strip('\'').strip('\"')
    return config_value



mail_host = get_config2('mail_server','smtp_host')
mail_port = int(get_config2('mail_server','smtp_port'))
mail_user = get_config2('mail_server','smtp_user')
mail_pass = get_config2('mail_server','smtp_pass')
mail_from = get_config2('mail_server','mail_from')


def send_mail(to_list,sub,content):
    '''
    to_list:发给谁
    sub:主题
    content:内容
    send_mail("aaa@126.com","sub","content")
    '''
    me=mail_from
    msg = MIMEText(content,'html', _charset='utf8')
    #msg = MIMEMultipart()
    #html_content = MIMEText(content,'html','utf-8') 
    #msg.attach(html_content)
    msg['Subject'] = Header(sub,'utf8')
    msg['From'] = Header(me,'utf8')
    msg['To'] = ";".join(to_list)
    try:
        smtp = smtplib.SMTP()
        smtp.connect(mail_host,mail_port)
        smtp.login(mail_user,mail_pass)
        smtp.sendmail(me,to_list, msg.as_string())
        smtp.close()
        return True
    except Exception, e:
        print "Send mail error: %s" %(str(e))
        return False
