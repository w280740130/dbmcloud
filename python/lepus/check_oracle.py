#!//bin/env python
#coding:utf-8
import os
import sys
import string
import time
import datetime
import MySQLdb
import cx_Oracle
import logging
import logging.config
logging.config.fileConfig("etc/logger.ini")
logger = logging.getLogger("lepus")
path='./include'
sys.path.insert(0,path)
import functions as func
import lib_oracle as oracle
import threading
import sendmail

db_type = 'oracle'
warning = 'warning'
critical = 'critical'
ok = 'ok'
connectivity = 'connectivity'
table_server = 'oracle_server'
table_status = 'oracle_status'
table_tbs = 'oracle_tablespace'
table_constraints = 'oracle_dba_constraints'
table_indexes = 'oracle_dba_indexes'
table_triggers = 'oracle_dba_triggers'
table_objects = 'oracle_dba_objects'
table_waits = 'oracle_session_waits'

mail_receiver_list = func.get_config('mail_server','mail_receiver_list')
sms_receiver_list = func.get_config('sms_server','sms_receiver_list')


def check_oracle(host,port,dsn,username,password,tags):
    url=host+':'+port+'/'+dsn
    server = func.get_current_server(table_server,host,port)
    send_mail = server[8]
    send_mail_to_list = server[9]
    send_sms = server[10]
    send_sms_to_list = server[11]
    alarm_session_total = server[12]
    alarm_session_actives = server[13]
    alarm_session_waits = server[14]
    alarm_tablespace = server[15]
    alarm_objects = server[16]
    alarm_index = server[17]
    alarm_hard_parse = server[18]
    alarm_sqlmonitor = server[19]
    alarm_rollstat = server[20]
    alarm_temp_tbs = server[21]
    alarm_data_dict_rate = server[22]
    alarm_buffer_pool_rate = server[23]
    alarm_table_hwm = server[24]
    alarm_white_list = server[25]
    threshold_session_total = server[26]
    threshold_session_actives = server[27]
    threshold_session_waits = server[28]
    threshold_tablespace = server[29]
    threshold_objects = server[30]
    threshold_index = server[31]
    threshold_hard_parse = server[32]
    threshold_sqlmonitor = server[33]
    threshold_rollstat = server[34]
    threshold_temp_tbs = server[35]
    threshold_data_dict_rate = server[36]
    threshold_buffer_pool_rate = server[37]
    threshold_table_hwm = server[38]
    threshold_white_list = server[39]
    now_time = func.get_now_time()

    if send_mail_to_list is None or  send_mail_to_list.strip()=='':
       send_mail_to_list = mail_receiver_list

    if send_sms_to_list is None or  send_sms_to_list.strip()=='':
       send_sms_to_list = sms_receiver_list

    try:
        #conn=cx_Oracle.connect(username,password,url) #获取connection对象
        tns=cx_Oracle.makedsn(host,port,dsn)
        conn=cx_Oracle.connect(username,password,tns) 
        logger.info("check oracle %s : connect success." %(url))
    except Exception, e:
        error_msg = str(e).strip('\n')
        logger_msg="check oracle %s : %s" %(url,error_msg)
        logger.warning(logger_msg)
	func.add_alarm(db_type,tags,host,port,now_time,connectivity,'error',critical,'connect error',error_msg,send_mail,send_sms)
        if send_mail==1:
            func.send_alarm_mail(db_type,tags,host,port,now_time,critical,'connect error',error_msg,send_mail_to_list)
         
        try:
            connect=0
            func.arch_data_to_history(table_status,host,port)
            sql="insert into oracle_status(host,port,tags,connect) values(%s,%s,%s,%s)"
            param=(host,port,tags,connect)
            func.mysql_exec(sql,param)
        except Exception, e:
            logger.error(str(e).strip('\n'))
            sys.exit(1)
        finally:
            sys.exit(1)
    
    finally:
        func.check_db_status(host,port,tags,'oracle')   

    try:
       session_total = oracle.get_sessions(conn)
       session_total_count = int(len(session_total))
       if int(alarm_session_total) == 1 and ( int(session_total_count) >= int(threshold_session_total) ):
          message = "%s session total"%(session_total_count)
          session_total_table = oracle.format_sessions_table(session_total)
          func.add_alarm(db_type,tags,host,port,now_time,'session total',session_total_count,warning,message,session_total_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,session_total_table,send_mail_to_list)
          if send_sms == 1:
             func.send_alarm_sms(db_type,tags,host,port,now_time,warning,message,send_sms_to_list)
    except Exception, e:
       logger.error(str(e).strip('\n'))
       sys.exit(1)


    try:
       session_actives = oracle.get_actives(conn)
       session_actives_count = int(len(session_actives))
       if int(alarm_session_actives) == 1 and ( int(session_actives_count) >= int(threshold_session_actives) ):
          message = "%s session actives"%(session_actives_count)
          session_actives_table = oracle.format_sessions_table(session_actives)
          func.add_alarm(db_type,tags,host,port,now_time,'session actives',session_actives_count,warning,message,session_actives_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,session_actives_table,send_mail_to_list)
          if send_sms == 1:
             func.send_alarm_sms(db_type,tags,host,port,now_time,warning,message,send_sms_to_list)
    except Exception, e:
       logger.error(str(e).strip('\n'))
       sys.exit(1)


    try:
       session_waits = oracle.get_waits(conn)
       session_waits_count = int(len(session_waits))
       if int(session_waits_count)>0 :
          func.truncate_current_data(table_waits,host,port)
          for line in session_waits:
             sql="insert into oracle_session_waits(host,port,tags,sid,serial,status,username,machine,module,event,wait_class,sql_id,sql_text,blocking_session,second_in_wait,state,terminal,program,os_user) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
             param=(host,port,tags,line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7],line[8],line[9],line[10],line[11],line[12],line[13],line[14],line[15])
             func.mysql_exec(sql,param)
       if int(alarm_session_waits) == 1 and ( int(session_waits_count) >= int(threshold_session_waits) ):
          message = "%s session waits"%(session_waits_count)
          session_waits_table = oracle.format_waits_table(session_waits)
          func.add_alarm(db_type,tags,host,port,now_time,'session waits',session_waits_count,warning,message,session_waits_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,session_waits_table,send_mail_to_list)
          if send_sms == 1:
             func.send_alarm_sms(db_type,tags,host,port,now_time,warning,message,send_sms_to_list)
    except Exception, e:
       logger.error(str(e).strip('\n'))
       sys.exit(1)

    try:
       objects_invalid = oracle.get_objects(conn)
       objects_invalid_count  = int(len(objects_invalid))
       func.truncate_current_data(table_objects,host,port)
       if int(objects_invalid_count)>0 :
          for line in objects_invalid:
             sql="insert into oracle_dba_objects(host,port,tags,owner,object_name, object_type,status) values(%s,%s,%s,%s,%s,%s,%s)"
             param=(host,port,tags,line[0],line[1],line[2],line[3])
             func.mysql_exec(sql,param)

       if int(alarm_objects) == 1 and ( int(objects_invalid_count)>=int(threshold_objects) ):
          message = "%s invalid objects"%(objects_invalid_count)
          objects_invalid_table = oracle.format_objects_table(objects_invalid)
          func.add_alarm(db_type,tags,host,port,now_time,'invalid objects',objects_invalid_count,warning,message,objects_invalid_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,objects_invalid_table,send_mail_to_list)

    except Exception, e:
       logger.error("monitor objects invalid error: %s" %(str(e).strip('\n')) )
       sys.exit(1)
    
    try:
       indexes_invalid = oracle.get_indexes(conn)
       indexes_invalid_count  = int(len(indexes_invalid))
       func.truncate_current_data(table_indexes,host,port)

       if int(indexes_invalid_count)>0 :
          for line in indexes_invalid:
             sql="insert into oracle_dba_indexes(host,port,tags,owner,index_name,index_type,table_owner,table_name,status,distinct_keys,num_rows,blevel) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
             param=(host,port,tags,line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7],line[8])
             func.mysql_exec(sql,param)

       if int(alarm_index) == 1 and ( int(indexes_invalid_count)>=int(threshold_index) ):
          message = "%s invalid indexes"%(indexes_invalid_count)
          indexes_invalid_table = oracle.format_indexes_table(indexes_invalid)
          func.add_alarm(db_type,tags,host,port,now_time,'invalid indexes',indexes_invalid_count,warning,message,indexes_invalid_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,indexes_invalid_table,send_mail_to_list)

    except Exception, e:
       logger.error("monitor indexes invalid error: %s" %(str(e).strip('\n')) )
       sys.exit(1)
 
   
    try:
       sqlmonitor = oracle.get_sqlmonitor(conn)
       sqlmonitor_count = int(len(sqlmonitor))
       if int(alarm_sqlmonitor) == 1 and ( int(sqlmonitor_count) >= int(threshold_sqlmonitor) ):
          message = "%s sql montor lag"%(sqlmonitor_count)
          sqlmonitor_table = oracle.format_sqlmonitor_table(sqlmonitor)
          func.add_alarm(db_type,tags,host,port,now_time,'sql monitor lag',sqlmonitor_count,warning,message,sqlmonitor_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,sqlmonitor_table,send_mail_to_list)
    except Exception, e:
       logger.error("monitor sqlmonitor error: %s" %(str(e).strip('\n')) )
       logger.error(str(e).strip('\n'))
       sys.exit(1)


    try:
       rollstat = oracle.get_rollstat(conn)
       rollstat_count = rollstat_segs_count = int(len(rollstat))
       if int(alarm_rollstat) == 1 and ( int(rollstat_count) >= int(threshold_rollstat) ):
          message = "%s rollstat active"%(rollstat_count)
          rollstat_table = oracle.format_rollstat_table(rollstat)
          func.add_alarm(db_type,tags,host,port,now_time,'rollstat active',rollstat_count,warning,message,rollstat_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,rollstat_table,send_mail_to_list)
    except Exception, e:
       logger.error("monitor rollstat error: %s" %(str(e).strip('\n')) )
       sys.exit(1)


    try:
       temp_sql = oracle.get_temp_tbs(conn)
       temp_sql_count = temp_tbs_sql_count = int(len(temp_sql))
       if int(alarm_temp_tbs) == 1 and ( int(temp_sql_count) >= int(threshold_temp_tbs) ):
          message = "%s temp sql"%(temp_sql_count)
          temp_sql_table = oracle.format_temp_tbs_table(temp_sql)
          func.add_alarm(db_type,tags,host,port,now_time,'temp sql',temp_sql_count,warning,message,temp_sql_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,temp_sql_table,send_mail_to_list)
    except Exception, e:
       logger.error("monitor temp tbs error: %s" %(str(e).strip('\n')) )
       sys.exit(1)
    
    try:
       abnormal_connections = oracle.get_abnormal_connections(conn,threshold_white_list)
    except Exception, e:
       logger.error("monitor white list error: %s" %(str(e).strip('\n')) )
       sys.exit(1)
    
    
    try:
        buffer_pool_rate = oracle.get_buffer_pool_rate(conn)
        buffer_pool_rate = buffer_pool_rate[4]
        if int(alarm_buffer_pool_rate) == 1 and ( buffer_pool_rate < threshold_buffer_pool_rate ):
          message = "%s buffer pool rate"%(buffer_pool_rate)
          func.add_alarm(db_type,tags,host,port,now_time,'buffer pool rate',buffer_pool_rate,warning,message,message,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,message,send_mail_to_list)
    except Exception, e:
       logger.error("monitor buffer pool rate error: %s" %(str(e).strip('\n')) )
       sys.exit(1)    

    try:
        data_dict_rate = oracle.get_data_dict_rate(conn)
        data_dict_rate = data_dict_rate[0]*100
        if int(alarm_data_dict_rate) == 1 and ( data_dict_rate < threshold_data_dict_rate ):
          message = "%s data dict rate"%(data_dict_rate)
          func.add_alarm(db_type,tags,host,port,now_time,'data dict rate',buffer_pool_rate,warning,message,message,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,message,send_mail_to_list)
    except Exception, e:
       logger.error("monitor data dict rate error: %s" %(str(e).strip('\n')) )
       sys.exit(1)


    try:
       table_hwm_all = oracle.get_table_hwm(conn)
       table_hwm = []
       for item in table_hwm_all:
          if int(item[6])>int(threshold_table_hwm):
             table_hwm.append(item)

       table_hwm_count  = int(len(table_hwm))
       if int(alarm_table_hwm) == 1 and ( int(table_hwm_count) >0 ):
          message = "%s table hwm high"%(table_hwm_count)
          table_hwm_table = oracle.format_table_hwm_table(table_hwm)
          func.add_alarm(db_type,tags,host,port,now_time,'table hwm',table_hwm_count,warning,message,table_hwm_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,table_hwm_table,send_mail_to_list)
    except Exception, e:
       logger.error("monitor table hwm error: %s" %(str(e).strip('\n')) )
       sys.exit(1)


    try:
        #get info by v$instance
        connect = 1
        instance_name = oracle.get_instance(conn,'instance_name')
        instance_role = oracle.get_instance(conn,'instance_role')
        database_role = oracle.get_database(conn,'database_role')
        open_mode = oracle.get_database(conn,'open_mode')
        protection_mode = oracle.get_database(conn,'protection_mode')
        if database_role == 'PRIMARY':  
            database_role_new = 'm'  
            dg_stats = '-1'
            dg_delay = '-1'
        else:  
            database_role_new = 's'
            #dg_stats = oracle.get_stats(conn)
            #dg_delay = oracle.get_delay(conn)
            dg_stats = '1'
            dg_delay = '1'

        instance_status = oracle.get_instance(conn,'status')
        startup_time = oracle.get_instance(conn,'startup_time')
        #print startup_time
        #startup_time = time.strftime('%Y-%m-%d %H:%M:%S',startup_time) 
        #localtime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
        #uptime =  (localtime - startup_time).seconds        
        #print uptime
        uptime = oracle.get_instance(conn,'startup_time')
        version = oracle.get_instance(conn,'version')
        instance_status = oracle.get_instance(conn,'status')
        database_status = oracle.get_instance(conn,'database_status')
        host_name = oracle.get_instance(conn,'host_name')
        archiver = oracle.get_instance(conn,'archiver')
        #get info by sql count
        session_total = session_total_count
        session_actives = session_actives_count
        session_waits = session_waits_count
        #get info by v$parameters
        parameters = oracle.get_parameters(conn)
        processes = parameters['processes']
        
        ##get info by v$parameters
        sysstat_0 = oracle.get_sysstat(conn)
        time.sleep(1)
        sysstat_1 = oracle.get_sysstat(conn)
        session_logical_reads_persecond = sysstat_1['session logical reads']-sysstat_0['session logical reads']
        physical_reads_persecond = sysstat_1['physical reads']-sysstat_0['physical reads']
        physical_writes_persecond = sysstat_1['physical writes']-sysstat_0['physical writes']
        physical_read_io_requests_persecond = sysstat_1['physical write total IO requests']-sysstat_0['physical write total IO requests']
        physical_write_io_requests_persecond = sysstat_1['physical read IO requests']-sysstat_0['physical read IO requests']
        db_block_changes_persecond = sysstat_1['db block changes']-sysstat_0['db block changes']
        os_cpu_wait_time = sysstat_0['OS CPU Qt wait time']
        logons_persecond = sysstat_1['logons cumulative']-sysstat_0['logons cumulative']
        logons_current = sysstat_0['logons current']
        opened_cursors_persecond = sysstat_1['opened cursors cumulative']-sysstat_0['opened cursors cumulative']
        opened_cursors_current = sysstat_0['opened cursors current']
        user_commits_persecond = sysstat_1['user commits']-sysstat_0['user commits']
        user_rollbacks_persecond = sysstat_1['user rollbacks']-sysstat_0['user rollbacks']
        user_calls_persecond = sysstat_1['user calls']-sysstat_0['user calls']
        db_block_gets_persecond = sysstat_1['db block gets']-sysstat_0['db block gets']

        #get current status value
        parse_time_cpu = sysstat_1['parse time cpu']
        parse_time_elapsed = sysstat_1['parse time elapsed']
        parse_count_total = sysstat_1['parse count (total)']
        parse_count_hard = sysstat_1['parse count (hard)']
        parse_count_failures = sysstat_1['parse count (failures)']
        
        #get last time status data
        last_data=func.mysql_query_one("select parse_time_cpu,parse_time_elapsed,parse_count_total,parse_count_hard,parse_count_failures from oracle_status where host='%s' and port='%s'" %(host,port))
        if last_data!=0:
           last_parse_time_cpu = last_data[0]
           last_parse_time_elapsed = last_data[1]
           last_parse_count_total = last_data[2]
           last_parse_count_hard = last_data[3]
           last_parse_count_failures = last_data[4]

           parse_time_cpu_inc = int(parse_time_cpu)-int(last_parse_time_cpu)
           parse_time_elapsed_inc = int(parse_time_elapsed)-int(last_parse_time_elapsed)
           parse_count_total_inc = int(parse_count_total)-int(last_parse_count_total)
           parse_count_hard_inc = int(parse_count_hard)-int(last_parse_count_hard)
           parse_count_failures_inc = int(parse_count_failures)-int(last_parse_count_failures)
        else:
           parse_time_cpu_inc = 0
           parse_time_elapsed_inc = 0
           parse_count_total_inc = 0
           parse_count_hard_inc = 0
           parse_count_failures_inc = 0
        

        #arch data to history
        func.arch_data_to_history(table_status,host,port)

        ##################### insert data to mysql server#############################
        sql = "insert into oracle_status(host,port,tags,connect,instance_name,instance_role,instance_status,database_role,open_mode,protection_mode,host_name,database_status,startup_time,uptime,version,archiver,session_total,session_actives,session_waits,dg_stats,dg_delay,processes,session_logical_reads_persecond,physical_reads_persecond,physical_writes_persecond,physical_read_io_requests_persecond,physical_write_io_requests_persecond,db_block_changes_persecond,os_cpu_wait_time,logons_persecond,logons_current,opened_cursors_persecond,opened_cursors_current,user_commits_persecond,user_rollbacks_persecond,user_calls_persecond,db_block_gets_persecond,parse_time_cpu,parse_time_elapsed,parse_count_total,parse_count_hard,parse_count_failures,parse_time_cpu_inc,parse_time_elapsed_inc,parse_count_total_inc,parse_count_hard_inc,parse_count_failures_inc,indexes_invalid_count,objects_invalid_count,sqlmonitor_count,rollstat_segs_count,temp_tbs_sql_count,data_dict_rate,buffer_pool_rate) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
        param = (host,port,tags,connect,instance_name,instance_role,instance_status,database_role,open_mode,protection_mode,host_name,database_status,startup_time,uptime,version,archiver,session_total,session_actives,session_waits,dg_stats,dg_delay,processes,session_logical_reads_persecond,physical_reads_persecond,physical_writes_persecond,physical_read_io_requests_persecond,physical_write_io_requests_persecond,db_block_changes_persecond,os_cpu_wait_time,logons_persecond,logons_current,opened_cursors_persecond,opened_cursors_current,user_commits_persecond,user_rollbacks_persecond,user_calls_persecond,db_block_gets_persecond,parse_time_cpu,parse_time_elapsed,parse_count_total,parse_count_hard,parse_count_failures,parse_time_cpu_inc,parse_time_elapsed_inc,parse_count_total_inc,parse_count_hard_inc,parse_count_failures_inc,indexes_invalid_count,objects_invalid_count,sqlmonitor_count,rollstat_segs_count,temp_tbs_sql_count,data_dict_rate,buffer_pool_rate)
        func.mysql_exec(sql,param) 
        func.update_db_status_init(database_role_new,version,host,port,tags)

        try:
           #check tablespace
           tablespace_all = oracle.get_tablespace_new(conn)
           if tablespace_all:
              #arch data to history
              func.arch_data_to_history(table_tbs,host,port)
              for line in tablespace_all:
                 tablespace_name = line[0]
                 total_size = line[1]
                 avail_size = line[2]
                 used_rate = line[3]
                 used_rate2 = used_rate+'%'
                 if total_size and avail_size:
                    used_size = total_size-avail_size
                 else:
                    used_size = -1
                 sql="insert into oracle_tablespace(host,port,tags,tablespace_name,total_size,used_size,avail_size,used_rate) values(%s,%s,%s,%s,%s,%s,%s,%s)"
                 param=(host,port,tags,tablespace_name,total_size,used_size,avail_size,used_rate)
                 func.mysql_exec(sql,param)

                 if int(alarm_tablespace) == 1 and ( round(float(used_rate))>=int(threshold_tablespace) ) and tablespace_name!='TEMP':
                    message = "%s tbs more than %s"%(tablespace_name,used_rate2)
                    func.add_alarm(db_type,tags,host,port,now_time,'tablespace',used_rate,warning,message,message,send_mail,send_sms)
                    if send_mail == 1:
                       func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,message,send_mail_to_list)
                    if send_sms == 1:
                       func.send_alarm_sms(db_type,tags,host,port,now_time,warning,message,send_sms_to_list)
        except Exception, e:
           logger.error("monitor tablespace error: %s" %(str(e).strip('\n')) )
           sys.exit(1) 

        
           

    except Exception, e:
        logger.error(e)
        sys.exit(1)

    finally:
        conn.close()
        




def main():

    servers=func.mysql_query("select host,port,dsn,username,password,tags from oracle_server where monitor=1;")
    logger.info("check oracle controller start.")

    if servers:
       threads = []
       i=0
       for item in servers:
          thread=threading.Thread(target=check_oracle, args=(item[0],item[1],item[2],item[3],item[4],item[5]))
          thread.setName(i)
          threads.append(thread)
          i=i+1
       for thread in threads:
          i = int(thread.getName())
          threads[i].start()
          time.sleep(3)
       for thread in threads:
          threads[i].join()

    else:
        logger.warning("check oracle: not found any servers")

    logger.info("check oracle controller finished.")
                     


if __name__=='__main__':
    main()
