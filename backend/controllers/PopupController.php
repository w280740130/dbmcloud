<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use common\models\Department;
use common\models\Position;
use backend\models\User;
use common\helpers\MyHelper;

class PopupController extends \yii\web\Controller
{
    /**
     * @return array
     */
    public function actionRead()
    {
        //if (Yii::$app->request->isAjax) {
            $type = Yii::$app->request->get('type');
            $id = Yii::$app->request->get('id');

            switch ($type) {
                case "department" :
                    $department = MyHelper::tree_to_list(MyHelper::list_to_tree(Department::find()->select(['id', 'parent_id', 'name'])->where(['organization_id' => Yii::$app->user->identity->organization_id])->orderBy('sort ASC')->asArray()->all(), $id));
                    $department = MyHelper::rotate($department);
                    $department = isset($department['id']) ? implode(",", $department['id']) . ",$id" : $id ;
                    //print_r($department);
                    $datalist = User::find()->join('LEFT JOIN','position','position.id=user.position')->select(['user.id','user.realname','user.username','position.name as position_name'])->where(['user.organization_id' => Yii::$app->user->identity->organization_id])->asArray()->all();
                case "position" :
                    break;

                default :
            }
            if (true) {// 读取成功
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'data' => $datalist,
                ];
            }
        //}
    }

}
