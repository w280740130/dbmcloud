<?php
namespace backend\controllers;

use backend\helpers\Pinyin;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use common\models\AccountLoginForm;
use common\models\SmsCodeLoginForm;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','captcha','check-mobile-exists'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' =>  [
                'class' => 'yii\captcha\CaptchaAction',
                'height' => 50,
                'width' => 120,
                'minLength' => 4,
                'maxLength' => 6
            ],
        ];
    }

    public function actionIndex()
    {
        if(!Yii::$app->user->can('site/index')) throw new ForbiddenHttpException(Yii::t('app', 'No Auth'));
		return $this->render('index');
    }

    public function actionCheckMobileExists(){
        $mobile = Yii::$app->request->post('mobile');
        if(User::findByMobile($mobile)){
            echo 'Success';
        }
        else{
            echo  'Failed';
        }
    }

    public function actionLogin()
    {

        $this->layout = 'guest';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->scenario = 'login_account';

        //判断登录提交后的登录方式
        if(Yii::$app->request->post()){
            $login_mode = Yii::$app->request->post()['LoginForm']['loginMode'];
            if($login_mode=='login_account'){
                $model = new LoginForm();
                $model->scenario = 'login_account';
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                    return $this->goBack();
                }
            }
            elseif($login_mode=='login_sms_code'){
                $model = new LoginForm();
                $model->scenario = 'login_sms_code';
                if ($model->load(Yii::$app->request->post()) && $model->sms_login()) {
                    return $this->goBack();
                }
                else{
                    var_dump($model->errors);exit;
                }
            }
        }


        //var_dump($model->errors);exit;
        return $this->render('login', ['model' => $model,]);

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
