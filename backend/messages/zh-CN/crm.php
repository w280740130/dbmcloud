<?php
/**
 * Created by PhpStorm.
 * User: funson
 * Date: 2014/10/25
 * Time: 10:33
 */

return [
    'STATUS_ACTIVE' => '启用',
    'STATUS_INACTIVE' => '禁用',
    'STATUS_DELETED' => '删除',
    'STATUS_DISPLAY' => '显示',
    'STATUS_HIDDEN' => '隐藏',

    'STATUS_TRACKING' => '跟踪',
    'STATUS_SUCCESS' => '成功',
    'STATUS_FAILED' => '失败',
    'STATUS_SHELVE' => '搁置',

    'STATUS_ON_SHELF' => '上架',
    'STATUS_OFF_SHELF' => '下架',

    'YES' => '是',
    'NO' => '否',

    'SEX_MALE' => '男',
    'SEX_FEMALE' => '女',
    'MAINTAIN_STATUS_FIRST' => '首次保养',
    'MAINTAIN_STATUS_REGULAR' => '定期保养',
    'PROMPT_STATUS' => '请筛选',
    'Please Filter' => '请筛选',
    'Please Select' => '请选择',
    'Please Select ' => '请选择',
    'No Option' => '无',
    'Not Set' => '(未设置)',
    'All' => '全部',

    'Operate' => '操作',
	'Create ' => '创建',
    'Create' => '创建',
    'Update ' => '更新',
    'Update' => '更新',
    'Delete' => '删除',
    'Delete ' => '删除',
    'Return' => '返回',
    'Return ' => '返回',
    'Print' => '打印',
    'Print ' => '打印',
    'Parent' => '上级',
    'Parent ' => '上级',
	'List' => '列表',
    'View' => '查看',
	'Manage' => '管理',
    'Select' => '选择',
    'Pass' => '审核通过',
    'First Pass' => '初审通过',
    'First Pass No' => '初审不通过',
    'Second Pass' => '复审通过',
    'Second Pass No' => '复审不通过',
    'Are you sure you want to delete this item?' => '您确定要删除此项吗？',
	'Are you sure you want to pass this item?' => '您确定要审核通过此项吗？',
    'Are you sure you want to deny this item?' => '您确定要不通过此项吗？',
    'Login' => '登录',
    'Incorrect username or password.' => '帐号或密码不正确',
    'Home' => '首页',
    'Logout' => '注销',
    'Online' => '在线',
    'Dashboard' => '控制面板',
    'The file "{file}" is not an image.' => '文件 "{file}" 不是一个图像文件。',
    'Hello, {name}' => '你好,{name}',
    'Search' => '搜索',
    'Reset' => '重置',

    'System' => '系统',
    'Settings' => '设置',
    'User' => '员工',
    'User ' => '员工',
    'Users' => '员工',
    'Role' => '角色',
    'Role ' => '角色',
    'Roles' => '角色',
    'Auth Role' => '角色',
    'Auth Roles' => '角色',
	'PERMISSION' => '权限',
	'Permission' => '权限',
    'Unique Code' => '唯一码',
    'Root Menu' => '根菜单',
	'Menu' => '菜单',
	'Menus' => '菜单',
	'Level' => '级别',
    'Parent' => '父级',
	'Parent Id' => '父级ID',
	'Route' => '路由',
	'Menu Icon' => '菜单图标',
	'Display' => '显示',
    'Uuid' => '唯一码',
    'Name' => '名称',
    'Subject' => '主题',
    'Description' => '描述',
    'Permissions' => '权限',
    'successfully saved' => '创建成功',
    'The Administrator has all permissions' => '超级管理员角色拥有所有权限',
	'The Administrator dose not delete' => '超级管理员角色被禁止删除',
    'successfully updated' => '更新成功',
    'successfully removed' => '删除成功',
    'still used' => '还在使用',

    'Username' => '用户名',
	'Realname' => '姓名',
    'Password' => '密码',
    'Repassword' => '确认密码',
    'Remember Me' => '自动登录',
    'SMS Code' => '短信验证码',
    'Get SMS Code' => '获取验证码',
    'Email' => '电子邮箱',
    'Role' => '角色',
    'Status' => '状态',
    'Change Password' => '修改密码',
    'Created At' => '创建时间',
    'Updated At' => '更新时间',
    'Created By' => '创建用户',
    'Updated By' => '更新用户',
	'No Auth' => '您未被授权执行此操作.',

    'Status' => '状态',
    'Time' => '时间',
    'Ip' => 'IP地址',
    'Action' => '行为',
    'Message' => '消息',
    'Default' => '内置',
    'Yes' => '是',
    'No' => '否',
    'Auto Generate' => '自动生成',
    'Initialization' => '初始化数据',
    'You Already Initialization' => '您已经执行过初始化数据.',

    'Backend Theme' => '后台主题',


    'Action Log' => '操作日志',
    'Action Logs' => '操作日志',

    'Directly Input Time' => '可直接输入日期，格式：2015-01-01',

    'Blog' => '博客',
    'Catalog' => '分类',
    'Post' => '文章',
    'Comment' => '评论',
    'Tag' => '标签',

    'Page' => '页面',

    'Category' => '分类',
    'Category ' => '分类',
    'Root Category' => '根分类',

    'The requested page does not exist.' => '您请求的页面不存在或已被删除.',
    'In using,Please delete the associated content.' => '您删除的项正在使用中，清先删除关联内容后再操作.',
    'You Are Not Create ' => '您还没有创建',
    'Parent category is not equal curent.' => '父级分类不能等于当前分类.',


    'OA Desktop'  => '办公桌面',

    //公告通知
    'Module'  => '模块',
    'Module '  => '模块',
    'Modules'  => '模块',
    'Price'  => '价格',
    'Promotion Price'  => '促销价',

    //通讯录
    'Organizational Structure'  => '组织架构',
    'Contacts'  => '通讯录',
    'Contacts '  => '通讯录',
    'Company' => '公司',
    'Category Name' => '分类名称',

    //公告通知
    'Notice'  => '公告',
    'Notice '  => '公告',
    'Notices'  => '公告',


    //收藏夹
    'Favorites ' => '收藏',
    'Favorites' => '收藏',
    'Favoriteses' => '收藏',
    'Favorites Category' => '收藏分类',
    'Favorites Categories' => '收藏分类',

    //日程安排
    'Schedule' => '日程安排',
    'Schedules' => '日程安排',
    'All Day' => '时间类型',
    'Begin Time' => '开始时间',
    'End Time' => '结束时间',
    'Remind' => '提醒',
    'Remind Time' => '提醒时间',
    'Participants' => '参与人员',
    'Message' => '消息',
    'Message ' => '消息',
    'Email' => '邮件',
    'Email ' => '邮件',
    'SMS' => '短信',
    'SMS ' => '短信',

    //行业分类
    'Industry Type' => '行业类型',
    'Industry Types' => '行业类型',
    //经济类型
    'Economic Type' => '经济类型',
    'Economic Types' => '经济类型',
    //销售方式
    'Sales Method' => '销售方式',
    'Sales Methods' => '销售方式',

    //销售阶段
    'Sales Stage' => '销售阶段',
    'Sales Stages' => '销售阶段',

    //客户级别
    'Customer Level' => '客户等级',
    'Customer Levels' => '客户等级',

    //客户来源
    'Customer Source' => '客户来源',
    'Customer Sources' => '客户来源',

    //证照类型
    'Cert Type' => '证照类型',
    'Cert Types' => '证照类型',



    //客户联系人
    'Customer Person' => '客户联系人',
    'Customer Persons' => '客户联系人',
    'Contact Person' => '联系人',
    'Contact Person ' => '联系人',
    'Birthday' => '生日',
    'Male' => '男',
    'Female' => '女',
    'Sex' => '性别',
    'Qq' => 'QQ号',
    'Mobile' => '手机',
    'Address' => '地址',
    'Website' => '个人网站',
    'Hobby' => '兴趣爱好',
    'First Time' => '建立沟通日期',
    'Tag' => '印象标签',
    'Tags' => '印象标签',
    'Count' => '计数器',


    //客户纪念日
    'Customer Memorial Days' => '客户纪念日',
    'Customer Memorial Day' => '客户纪念日',
    'Memorial Name' => '纪念日类型',
    'Memorial Date' => '纪念日时间',

    //供应商级别
    'Supplier Level' => '供应商级别',
    'Supplier Levels' => '供应商级别',

    //银行卡管理
    'Bank Account' => '银行账户',
    'Bank Accounts' => '银行账户',
    'Bank Name' => '银行开户行',
    'Account' => '银行账号',
    'Type' => '类型',
    'Owner' => '隶属人',

    //计量单位
    'Measurement Unit' => '计量单位',
    'Measurement Units' => '计量单位',
    'Measurement Units ' => '计量单位',
    //商品
    'Goods' => '商品',
    'Goodses' => '商品',
    'Goods ' => '商品',
    'Goods Code' => '商品编码',
    'Goods Specifications' => '商品规格',
    'Goods Model' => '商品型号',
    'Sales Price' => '销售价格',
    'Member Price' => '会员价格',
    'Vip Price' => 'VIP价格',
    'Original Code' => '原厂码',
    //商品分类
    'Goods Category' => '商品分类',
    'Goods Categories' => '商品分类',
    'Goods Category ' => '商品分类',
    //商品品牌
    'Logo' => 'LOGO',
    'Brand' => '品牌',
    'Brand ' => '品牌',
    'Chinese' => '中文',
    'Chinese ' => '中文',
    'English' => '英文',
    'English ' => '英文',
    'Goods Brand' => '商品品牌',
    'Goods Brands' => '商品品牌',
    'Goods Brand ' => '商品品牌',

    //单位信息
    'Organization' => '单位信息',
    'Org Name' => '单位名称',
    'Org Phone' => '单位电话',
    'Org Fax' => '传真号码',
    'Org Email' => '电子邮箱',
    'Org Url' => '单位网站',
    'Org Address' => '通讯地址',

    //部门信息
    'Root Department' => '顶级部门',
    'Department ' => '部门',
    'Department' => '组织部门',
    'Departments' => '组织部门',
    'You Are No Department Yet!' => '您还没有创建组织部门!',

    //职位
    'Position ' => '职位',
    'Position' => '职位',
    'Positions' => '职位',
    'Leader' => '领导',
    'Parent Leader' => '上级领导',
	
	//客户基本信息
	'Customer' => '客户',
	'Customers' => '客户',
    'Customer ' => '客户',
	'Customer Basic' => '客户基本信息',
	'Customer Attribute' => '客户属性信息',
	'Customer Addition' => '客户附加信息',
	'Automatic Calculation' => '自动计算',
    'Telphone' => '电话',
    'Fax' => '传真',
    'Url' => '网址',
    'Province' => '省份',
    'City' => '城市',
    'District' => '区域',
    'Private' => '私有',
    'Bank Name' => '银行开户行',
    'Bank Bunmber' => '银行账户',
    'Remark' => '备注信息',



    //供应商信息
    'Supplier Basic' => '供应商基本信息',
    'Supplier Contact' => '供应商联系信息',
    'Supplier Addition' => '供应商附加信息',
    'Supplier ' => '供应商',
    'Supplier' => '供应商',
    'Suppliers' => '供应商',
    'Business Scope' => '经营业务范围',
    'Buyer' => '采购员',

    //供应商联系人
    'Supplier Person' => '供应商联系人',
    'Supplier Persons' => '供应商属性信息',

    //供应商证件照
    'Supplier Cert' => '供应商证件照',
    'Supplier Certs' => '供应商证件照',
    'Cert Number' => '证件号码',
    'Cert Number' => '证件号码',
    'Expiry Date Begin' => '有效期开始日期',
    'Expiry Date End' => '有效期结束日期',
    'Cert' => '证书',
    'Cert ' => '证书',
    'Photo' => '照片',

    //销售机会
    'Sales Chance' => '销售机会',
    'Sales Chances' => '销售机会',
    'Sales Chance ' => '销售机会',
    'Discovery Time' => '发现时间',
    'Demand' => '客户需求',
    'Expected Time' => '预计签单时间',
    'Expected Expenses' => '预计签单金额',
    'Possibility' => '可能性',
    'Current Generation' => '当前阶段',
    'Current Generation' => '当前阶段',

    'Supplier Offer' => '供应商报价',
    'Supplier Offer ' => '供应商报价',
    'Supplier Offers' => '供应商报价',

];