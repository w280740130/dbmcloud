<?php
/**
 * Created by PhpStorm.
 * User: funson
 * Date: 2014/10/25
 * Time: 10:33
 */

return [
    'STATUS_ACTIVE' => '启用',
    'STATUS_INACTIVE' => '禁用',
    'STATUS_DELETED' => '删除',
    'STATUS_DISPLAY' => '显示',
    'STATUS_HIDDEN' => '隐藏',

    'STATUS_TRACKING' => '跟踪',
    'STATUS_SUCCESS' => '成功',
    'STATUS_FAILED' => '失败',
    'STATUS_SHELVE' => '搁置',

    'STATUS_ON_SHELF' => '上架',
    'STATUS_OFF_SHELF' => '下架',

    'YES' => '是',
    'NO' => '否',

    'ON' => '开启',
    'OFF' => '关闭',

    'SEX_MALE' => '男',
    'SEX_FEMALE' => '女',
    'MAINTAIN_STATUS_FIRST' => '首次保养',
    'MAINTAIN_STATUS_REGULAR' => '定期保养',
    'PROMPT_STATUS' => '请筛选',
    'Please Filter' => '请筛选',
    'Please Select' => '请选择',
    'Please Select ' => '请选择',
    'No Option' => '无',
    'Not Set' => '(未设置)',
    'All' => '全部',

    'Operate' => '操作',
	'Create ' => '创建',
    'Create' => '创建',
    'Update ' => '更新',
    'Update' => '更新',
    'Delete' => '删除',
    'Delete ' => '删除',
    'Return' => '返回',
    'Return ' => '返回',
    'Print' => '打印',
    'Print ' => '打印',
    'Parent' => '上级',
    'Parent ' => '上级',
	'List' => '列表',
    'View' => '查看',
    'Sort' => '排序',
	'Manage' => '管理',
    'Select' => '选择',
    'Pass' => '审核通过',
    'First Pass' => '初审通过',
    'First Pass No' => '初审不通过',
    'Second Pass' => '复审通过',
    'Second Pass No' => '复审不通过',
    'Are you sure you want to delete this item?' => '您确定要删除此项吗？',
	'Are you sure you want to pass this item?' => '您确定要审核通过此项吗？',
    'Are you sure you want to deny this item?' => '您确定要不通过此项吗？',
    'Login' => '登录',
    'Incorrect username or password.' => '帐号或密码不正确',
    'Home' => '首页',
    'Logout' => '注销',
    'Online' => '在线',
    'Dashboard' => '控制面板',
    'The file "{file}" is not an image.' => '文件 "{file}" 不是一个图像文件。',
    'Hello, {name}' => '你好,{name}',
    'Search' => '搜索',
    'Reset' => '重置',
    'Sort Order' => '排序',

    'System' => '系统',
    'Settings' => '设置',
    'User' => '员工',
    'User ' => '员工',
    'Users' => '员工',
    'Role' => '角色',
    'Role ' => '角色',
    'Roles' => '角色',
    'Auth Role' => '角色',
    'Auth Roles' => '角色',
	'PERMISSION' => '权限',
	'Permission' => '权限',
    'Unique Code' => '唯一码',
    'Root Menu' => '根菜单',
	'Menu' => '菜单',
	'Menus' => '菜单',
	'Level' => '级别',
    'Parent' => '父级',
	'Parent Id' => '父级ID',
	'Route' => '路由',
	'Menu Icon' => '菜单图标',
	'Display' => '显示',
    'Uuid' => '唯一码',
    'Name' => '名称',
    'Subject' => '主题',
    'Description' => '描述',
    'Permissions' => '权限',
    'successfully saved' => '创建成功',
    'The Administrator has all permissions' => '超级管理员角色拥有所有权限',
	'The Administrator dose not delete' => '超级管理员角色被禁止删除',
    'successfully updated' => '更新成功',
    'successfully removed' => '删除成功',
    'still used' => '还在使用',

    'Username' => '用户名',
	'Realname' => '姓名',
    'Mobile' => '手机号',
    'Telphone' => '电话',
    'Password' => '密码',
    'Repassword' => '确认密码',
    'Remember Me' => '自动登录',
    'SMS Code' => '短信验证码',
    'Get SMS Code' => '获取验证码',
    'Email' => '电子邮箱',
    'Role' => '角色',
    'Status' => '状态',
    'Change Password' => '修改密码',
    'Created At' => '创建时间',
    'Updated At' => '更新时间',
    'Created By' => '创建用户',
    'Updated By' => '更新用户',
	'No Auth' => '您未被授权执行此操作.',

    'Status' => '状态',
    'Time' => '时间',
    'Ip' => 'IP地址',
    'Action' => '行为',
    'Message' => '消息',
    'Default' => '内置',
    'Yes' => '是',
    'No' => '否',
    'Auto Generate' => '自动生成',
    'Initialization' => '初始化数据',
    'You Already Initialization' => '您已经执行过初始化数据.',

    'Backend Theme' => '后台主题',
    'Enable Modules' => '启用模块',


    'Action Log' => '操作日志',
    'Action Logs' => '操作日志',

    'Directly Input Time' => '可直接输入日期，格式：2015-01-01',

    'Blog' => '博客',
    'Catalog' => '分类',
    'Post' => '文章',
    'Comment' => '评论',
    'Tag' => '标签',

    'Page' => '页面',

    'Category' => '分类',
    'Category ' => '分类',
    'Root Category' => '根分类',

    'The requested page does not exist.' => '您请求的页面不存在或已被删除.',
    'In using,Please delete the associated content.' => '您删除的项正在使用中，清先删除关联内容后再操作.',
    'You Are Not Create ' => '您还没有创建',
    'Parent category is not equal curent.' => '父级分类不能等于当前分类.',


    'OA Desktop'  => '办公桌面',

    //公告通知
    'Module'  => '模块',
    'Module '  => '模块',
    'Modules'  => '模块',
    'Price'  => '价格',
    'Promotion Price'  => '促销价',
    'Private' => '私有',



    //单位信息
    'Organization' => '单位信息',
    'Org Name' => '单位名称',
    'Org Phone' => '单位电话',
    'Org Fax' => '传真号码',
    'Org Email' => '电子邮箱',
    'Org Url' => '单位网站',
    'Org Address' => '通讯地址',

    //部门信息
    'Root Department' => '顶级部门',
    'Department ' => '部门',
    'Department' => '组织部门',
    'Departments' => '组织部门',
    'You Are No Department Yet!' => '您还没有创建组织部门!',

    //职位
    'Position ' => '职位',
    'Position' => '职位',
    'Positions' => '职位',
    'Leader' => '领导',
    'Parent Leader' => '上级领导',


];