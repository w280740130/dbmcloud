<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "action_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $time
 * @property string $ip
 * @property string $action
 * @property string $message
 */
class ActionLog extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action_log';
    }

    /**
     * create_time, update_time to now()
     * crate_user_id, update_user_id to current login user id
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            // BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'action'], 'required'],
            [['id', 'user_id','organization_id'], 'integer'],
            [['time'], 'safe'],
            [['ip', 'action', 'message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'time' => Yii::t('app', 'Time'),
            'ip' => Yii::t('app', 'Ip'),
            'action' => Yii::t('app', 'Action'),
            'message' => Yii::t('app', 'Message'),
        ];
    }

    /**
     * Before save.
     * 
     */
    /*public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            // add your code here
            return true;
        }
        else
            return false;
    }*/

    /**
     * After save.
     *
     */
    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
    }*/

    public function getRealIp()
    {
        $ip=false;
        if(!empty($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
            if ($ip) { array_unshift($ips, $ip); $ip = FALSE; }
            for ($i = 0; $i < count($ips); $i++) {
                if (!eregi ("^(10│172.16│192.168).", $ips[$i])) {
                    $ip = $ips[$i];
                    break;
                }
            }
        }
        return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
    }

    public static  function log($message){
        $user_id = Yii::$app->user->id;
        $action = Yii::$app->controller->id.'/'.Yii::$app->requestedAction->id;
        $ip = $_SERVER["REMOTE_ADDR"];
        $model = new ActionLog();
        $model->user_id = $user_id;
        $model->ip = $ip;
        $model->action = $action;
        $model->message = $message;
        $model->organization_id = Yii::$app->user->identity->organization_id;
        if($model->validate()){
            $model->save();
        }
        else{
            print_r($model->errors);exit;
        }
    }

    public function getUser(){
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }



}
