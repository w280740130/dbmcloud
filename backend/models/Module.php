<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "module".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property integer $private
 * @property integer $system
 * @property integer $price
 * @property integer $promotion_price
 * @property integer $sort_order
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Module extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module';
    }

    /**
     * create_time, update_time to now()
     * crate_user_id, update_user_id to current login user id
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            // BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['private', 'system', 'sort_order', 'status', 'created_at', 'updated_at','price','promotion_price'], 'integer'],
            [['name', 'title'], 'string', 'max' => 30],
            [['description'], 'string', 'max' => 100],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Module ').Yii::t('app', 'Name'),
            'title' => Yii::t('app', 'Module ').Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'private' => Yii::t('app', 'Private'),
            'system' => Yii::t('app', 'System'),
            'price' => Yii::t('app', 'Price'),
            'promotion_price' => Yii::t('app', 'Promotion Price'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Before save.
     * 
     */
    /*public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            // add your code here
            return true;
        }
        else
            return false;
    }*/

    /**
     * After save.
     *
     */
    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
    }*/

    public static function findTotal(){
        return static::find()->select(['id','title'])->where(['status'=>1])->orderBy('sort_order ASC')->asArray()->all();
    }

    public static function getNoSysModules(){
        return static::find()->select(['id','title'])->where(['status'=>1,'system'=>0])->orderBy('sort_order ASC')->asArray()->all();
    }

    public static function getTitle($id){
        if($id && is_numeric($id)){
            $result = static::find()->select(['id','title'])->where(['id'=>$id])->one();
            if(isset($result)){
                return $result->title;
            }
            else{
                return Yii::t('app', 'Not Set');
            }
        }
        return Yii::t('app', 'Not Set');
    }

    public static function getId($name){
            $result = static::find()->select(['id','title'])->where(['name'=>$name])->one();
            if(isset($result)){
                return $result->id;
            }
            else{
                return Yii::t('app', 'Not Set');
            }
    }

    static public function getAvailModule(){

        $organization_id = Yii::$app->user->identity->organization_id;
        $enable_modules_ids = Yii::$app->settings->get('enable_modules');
        if($organization_id === 1){
            return array_merge(explode(',',$enable_modules_ids),[1,2]);
        }
        else{
            return array_merge(explode(',',$enable_modules_ids),[1]);
        }


    }

}
