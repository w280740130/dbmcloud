<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Favorites */

$this->title = Yii::t('app', 'Update ') . Yii::t('app', 'Favorites') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Favorites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="favorites-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
