<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Organization;

/* @var $this yii\web\View */
/* @var $model common\models\Organization */
/* @var $form ActiveForm */

$this->title = '单位管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="organization-index">
    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'phone') ?>
        <?= $form->field($model, 'fax') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'url') ?>
        <?= $form->field($model, 'address') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- organization-index -->
