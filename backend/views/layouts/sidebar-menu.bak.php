<?php
use common\widgets\Menu;

echo Menu::widget(
    [
        'options' => [
            'class' => 'sidebar-menu'
        ],
        'items' => [
            [
                'label' => '系统首页',
                'url' => Yii::$app->homeUrl,
                'icon' => 'fa-dashboard',
                'active' => Yii::$app->request->url === Yii::$app->homeUrl
            ],
			[
                'label' => '系统设置',
                'url' => ['/setting/index'],
                'icon' => 'fa-dashboard',
                'active' => Yii::$app->request->url === Yii::$app->request->baseUrl.'/setting/index',
				'visible' => Yii::$app->user->can('setting/index'),
            ],
			
            [
                'label' => '用户和权限',
                'url' => ['#'],
                'icon' => 'fa fa-cog',
				'visible' => Yii::$app->user->can('users'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [
                    [
                        'label' => '用户管理',
                        'url' => ['/user/index'],
                        'icon' => 'fa fa-user',
                        //'visible' => (Yii::$app->user->identity->username == 'admin'),
						'visible' => Yii::$app->user->can('user/index'),
                    ],
                    [
                        'label' => '角色管理',
                        'url' => ['/role/index'],
                        'icon' => 'fa fa-lock',
						'visible' => Yii::$app->user->can('role/index'),
                    ],
					[
                        'label' => '权限管理',
                        'url' => ['/permission/index'],
                        'icon' => 'fa fa-lock',
						'visible' => Yii::$app->user->can('permission/index'),
                    ],
					[
                        'label' => '菜单管理',
                        'url' => ['/menu/index'],
                        'icon' => 'fa fa-lock',
						'visible' => Yii::$app->user->can('menu/index'),
                    ],
                ],
            ],
			
			[
                'label' => '客户管理平台',
                'url' => ['#'],
                'icon' => 'fa fa-cog',
				'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [
                    [
                        'label' => '客户信息管理',
                        'url' => ['/crm-customer/index'],
                        'icon' => 'fa fa-user',
						'visible' => Yii::$app->user->can('crm-customer/index'),
                    ],
					[
                        'label' => '客户收益管理',
                        'url' => ['/crm-earnings/index'],
                        'icon' => 'fa fa-user',
						'visible' => Yii::$app->user->can('crm-earnings/index'),
                    ],
                   
                ],
            ],
			
			[
                'label' => '网站管理平台',
                'url' => ['#'],
                'icon' => 'fa fa-cog',
				'visible' => Yii::$app->user->can('customerRead'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [
                    [
                        'label' => '栏目管理',
                        'url' => ['/cms-catalog/index'],
                        'icon' => 'fa fa-user',
                        //'visible' => (Yii::$app->user->identity->username == 'admin'),
						'visible' => true,
                    ],
					[
                        'label' => '文章管理',
                        'url' => ['/cms-article/index'],
                        'icon' => 'fa fa-user',
                        //'visible' => (Yii::$app->user->identity->username == 'admin'),
						'visible' => true,
                    ],
					[
                        'label' => '页面管理',
                        'url' => ['/cms-page/index'],
                        'icon' => 'fa fa-user',
                        //'visible' => (Yii::$app->user->identity->username == 'admin'),
						'visible' => Yii::$app->user->can('customerRead'),
                    ],
                   
                ],
            ],
			
			[
                'label' => '微信管理平台',
                'url' => ['#'],
                'icon' => 'fa fa-cog',
				'visible' => Yii::$app->user->can('customerRead'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [
                    [
                        'label' => '微信公众号配置',
                        'url' => ['/wechat/index'],
                        'icon' => 'fa fa-user',
						'visible' => TRUE,
                    ],
					[
                        'label' => '微信菜单管理',
                        'url' => ['/wechat-menu/index'],
                        'icon' => 'fa fa-user',
						'visible' => TRUE,
                    ],
					[
                        'label' => '微信H5页面管理',
                        'url' => ['/wechat-html/index'],
                        'icon' => 'fa fa-user',
						'visible' => TRUE,
                    ],
                   
                ],
				
            ],

        ]
    ]
);