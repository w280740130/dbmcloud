
<ul class="nav navbar-nav">

    <?php
    $newAlarm = \backend\modules\alarm\models\Alarm::findNewAlarm();
    $newAlarmCount  = count($newAlarm);
    ?>
    <!-- Notifications: style can be found in dropdown.less -->
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span class="label label-warning"><?php echo $newAlarmCount;?></span>
        </a>
        <ul class="dropdown-menu" style="width: 560px;">
            <li class="header">最新告警</li>
            <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                    <?php foreach($newAlarm as $item) : ?>
                    <li>
                        <a href="<?= \Yii::$app->urlManager->createUrl(['alarm/default/view','id'=>$item['id'] ]); ?>">
                            <i class="fa fa-warning text-yellow"></i> <?php echo $item['create_time'].' '.$item['db_type'].'-'.$item['host'].':'.$item['port'].'/'.$item['tags'].' : '.$item['message'] ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <li class="footer"><a href="<?= \Yii::$app->urlManager->createUrl(['alarm/default/index']); ?>">查看所有告警</a></li>
        </ul>
    </li>

    <!-- User Account: style can be found in dropdown.less -->
    <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?= Yii::getAlias('@web'); ?>/images/avatar-male.png " class="user-image" alt="User Image" />
            <span class="hidden-xs">管理员</span>
        </a>
        <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
                <img src="<?= Yii::getAlias('@web'); ?>/images/avatar-male.png " class="img-circle" alt="User Image" />
                <p>
                    业务部门-总监
                    <small>Member since Nov. 2012</small>
                </p>
            </li>

            <!-- Menu Footer-->
            <li class="user-footer">
                <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">修改密码</a>
                </div>
                <div class="pull-right">
                    <a href="/site/logout" data-method="post" class="btn btn-default btn-flat">安全退出</a>
                </div>
            </li>
        </ul>
    </li>

</ul>