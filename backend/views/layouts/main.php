<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use c006\alerts\Alerts;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<?php

if(Yii::$app->settings->get('backend_theme')!=null){
    $backend_theme = Yii::$app->settings->get('backend_theme');
}
else{
    $backend_theme = 'skin-green-light';
}

?>
<body class=" <?= $backend_theme  ?> ">
<div class="shade"></div>
<?php $this->beginBody() ?>
<div class="wrapper">
    <header class="main-header">
        <a href="<?= Yii::$app->homeUrl ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>JZ</b>A</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><?= Yii::$app->name ?></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only"><?= Yii::t('app', 'Toggle navigation') ?></span>
            </a>
            <div class="navbar-custom-menu" style="float: left;">
                <?= $this->render('//layouts/top-menu.php') ?>
            </div>
            <div class="navbar-custom-menu">
            <?= $this->render('//layouts/top-menu-right.php') ?>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar ">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?= Yii::getAlias('@web'); ?>/images/avatar-male.png" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>
                        <?= Yii::t('app', 'Hello, {name}', ['name' => Yii::$app->user->identity->username]) ?>
                    </p>
                    <a>
                        <i class="fa fa-circle text-success"></i> <?= Yii::t('app', 'Online') ?>
                    </a>
                </div>
            </div>
            <!-- search form -->
            <!--
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..." />
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
                </div>
            </form>
            -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?= $this->render('//layouts/sidebar-menu') ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $this->title ?>
                <?php if (isset($this->params['subtitle'])) : ?>
                    <small><?= $this->params['subtitle'] ?></small>
                <?php endif; ?>
            </h1>
            <?= Breadcrumbs::widget(
                [
                    'homeLink' => [
                        'label' => '<i class="fa fa-dashboard"></i> ' . Yii::t('app', 'Home'),
                        'url' => ['/']
                    ],
                    'encodeLabels' => false,
                    'tag' => 'ol',
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
                ]
            ) ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?= Alerts::widget(['countdown' => 5]); ?>
            <?= Alert::widget(); ?>
            <?= $content ?>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!--
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            Version 2.2.0
        </div>
        Copyright &copy; 2015 <a href="http://www.jz-zc.com">JZ Asset</a>. All rights reserved.
    </footer>
    -->

    <?php   echo $this->render('//layouts/control-sidebar.php') ?>

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
