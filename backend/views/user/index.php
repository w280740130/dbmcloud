<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use \common\models\Department;
use yii\helpers\ArrayHelper;
use forecho\jqtree\JQTree;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <p>
                <?= Html::a(Yii::t('app', 'Create ') . Yii::t('app', 'User'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    /*[
                        'attribute' => 'username',
                        'headerOptions' => ['width'=>'100'],
                    ],*/
                    [
                        'attribute' => 'realname',
                        'headerOptions' => ['width'=>'100'],
                    ],
                    [
                        'attribute' => 'mobile',
                        'headerOptions' => ['width'=>'110'],
                    ],
                    [
                        'attribute' => 'telphone',
                        'headerOptions' => ['width'=>'110'],
                    ],

                    'email:email',

                    [
                        'attribute' => 'department',
                        'value' => function ($model) {
                            return \common\models\Department::getName($model->department);
                        },
                        'filter' => Html::activeDropDownList(
                            $searchModel,
                            'department',
                             ArrayHelper::map(Department::getTree(0, Department::getAll()), 'id', 'name'),
                            ['class' => 'form-control', 'prompt' => Yii::t('app', 'Please Filter')]
                        ),
                        'headerOptions' => ['width'=>'150'],
                    ],
                    [
                        'attribute' => 'position',
                        'headerOptions' => ['width' => '130'],
                        'value' => function ($model) {
                            return \common\models\Position::getName($model->position);
                        },
                        'filter' => Html::activeDropDownList(
                            $searchModel,
                            'position',
                            ArrayHelper::map(\common\models\Position::findTotal(), 'id', 'name'),
                            ['class' => 'form-control select-filter', 'prompt' => Yii::t('app', 'Please Filter')]
                        ),

                    ],
                    [
                        'attribute' => 'role',
                        'value' => function ($model) {
                            return $model->getRoleName($model->role);
                        },
                        'filter' => Html::activeDropDownList(
                            $searchModel,
                            'role',
                            ArrayHelper::map(User::getRole(), 'name', 'description'),
                            ['class' => 'form-control', 'prompt' => Yii::t('app', 'Please Filter')]
                        ),
                        'headerOptions' => ['width'=>'130'],
                    ],

                    [
                        'attribute' => 'status',
                        'format' => 'html',
                        'value' => function ($model) {
                            if ($model->status === $model::STATUS_ACTIVE) {
                                $class = 'label-success';
                            } elseif ($model->status === $model::STATUS_INACTIVE) {
                                $class = 'label-warning';
                            } else {
                                $class = 'label-danger';
                            }

                            return '<span class="label ' . $class . '">' . $model->statusLabel . '</span>';
                        },
                        'filter' => Html::activeDropDownList(
                            $searchModel,
                            'status',
                            $arrayStatus,
                            ['class' => 'form-control', 'prompt' => Yii::t('app', 'Please Filter')]
                        ),
                        'headerOptions' => ['width'=>'120'],
                    ],

                    [
                        'label'=>Yii::t('app', 'Operate'),
                        'headerOptions' => ['width'=>'70'],
                        'format'=>'raw',
                        'value' => function($model){
                            if($model->admin==1){
                                return   '禁止';

                            }else{
                                return
                                    Html::a('<span class="glyphicon glyphicon-eye-open"></span>',Yii::$app->getUrlManager()->createUrl(['user/view','auth_key'=>$model['auth_key']]), ['title' => Yii::t('app','View') ]).'&nbsp'
                                    .Html::a('<span class="glyphicon glyphicon-pencil"></span>',Yii::$app->getUrlManager()->createUrl(['user/update','auth_key'=>$model['auth_key']]), ['title' => Yii::t('app','Update') ]).'&nbsp'
                                    .Html::a('<span class="glyphicon glyphicon-trash"></span>',Yii::$app->getUrlManager()->createUrl(['user/delete','auth_key'=>$model['auth_key']]), ['title' => Yii::t('app','Delete') ],$options=['data-method' => 'post','data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?')] );
                            }
                        }
                    ],

                ],
            ]); ?>

</div>
