<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->realname.'('.$model->username.')';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('app', 'Return'), ['index'], ['class' => 'btn btn-warning']) ?>
        <?= Html::button(Yii::t('app', 'Print'), ['class' => 'btn btn-info','onclick'=>'preview()']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'auth_key' => $model->auth_key], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'auth_key' => $model->auth_key], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <!--startprint-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'username',
			'realname',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'mobile',
            'email:email',
            [
                'attribute' => 'role',
                'value' => $model->getRoleName($model->role)
            ],
            ['label'=>Yii::t('app','Department'),'value'=>\common\models\Department::getName($model->department)],
            ['label'=>Yii::t('app','Position'),'value'=>\common\models\Position::getName($model->position)],
            ['label'=>Yii::t('app','Parent Leader'),'value'=>\backend\models\User::getRealname($model->leader)],
            [
                'attribute' => 'status',
                'value' => $model->statusLabel,
            ],
            'created_at:datetime',
            'updated_at:datetime',
            ['label'=>Yii::t('app','Created By'),'value'=>\backend\models\User::getRealname($model->created_by)],
            ['label'=>Yii::t('app','Updated By'),'value'=>\backend\models\User::getRealname($model->updated_by)],
        ],
    ]) ?>
    <!--endprint-->

</div>
