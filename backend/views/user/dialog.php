<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use \common\models\Department;
use yii\helpers\ArrayHelper;
use forecho\jqtree\JQTree;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = '';

?>

<div class="panel panel-default ">
    <div class="panel-heading clearfix">
        <div class="pull-left">
            <label>
                <input class="ace" type="radio" id="rd_department" name="type" value="department">
                <span class="lbl">部门</span> </label>

            <label>
                <input class="ace" type="radio" id="rd_position" name="type" value="position">
                <span class="lbl">职位</span> </label>
        </div>
        <div class="pull-right">
            <a onclick="save();" class="btn btn-sm btn-primary">确定</a>
            <a onclick="winclose();" class="btn btn-sm btn-danger">关闭</a>
        </div>
    </div>
    <div class="panel-body">
        <div class="col-lg-6 pull-left" style="width: 300px;">
            <div class="">
                <b>地址簿</b>
            </div>
            <div class="popup_tree_menu">
                <div id="department" class="display-none" style="height:200px;">
                    <?= $departmentTreeMenu ?>
                </div>
                <div id="position" class="display-none" style="height:200px;">
                    22222222222222222
                </div>
            </div>
            <div class="clear"></div>
            <div>
                <div id="addr_list" style="width:100%;height:210px;"></div>
            </div>
        </div>
        <div class="col-lg-4 pull-right" style="width: 320px;">
            <div>
                <b style="padding-left: 60px;">已选择</b><span id="rc_count"></span>
            </div>
            <div class="clearfix" style="margin-bottom: 15px;">
                <label class="col-4 pull-left text-right" style="margin-top: 200px;">
                    <a onclick="add_address('rc');" class="btn btn-sm btn-primary">
                        <i class="fa fa-angle-double-right"></i> </a> </label>

                <div class=" pull-right" style="width: 230px;">
                    <div id="rc" style="width:100%;height:420px;overflow:hidden">
                        <select size="6" style="height:100%;width:100%;"></select>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        //加载默认类型
        $("#rd_department").prop('checked', true);
        // 选择用户默认选择的类型
        $("#department").removeClass("display-none");

        $("input[name='type']").on('click', function () {
            $("input[name='type']").each(function () {
                $("#" + $(this).val()).addClass("display-none");
            });
            $("#" + $(this).val()).removeClass("display-none")
        })

        //点击节点时读取相应的数据
        $(".tree_menu  a").click(function () {
            $(".tree_menu a").removeClass("active");
            var type = $("input[name='type']:checked").val();
            $(this).addClass("active");
            $.ajax({
                async : false,
                cache : false,
                type : 'GET',
                url : "/popup/read?type=" + type + "&id=" + $(this).attr("node"),// 请求的action路径
                data:'',
                //dataType : "json",
                error : function() {// 请求失败处理函数
                    alert('请求处理失败，请重试。');
                },
                success:function(result){
                   showdata(result);
                }
            });

            return false;
            //禁止连接生效
        });

        function showdata(result) {
            $("#addr_list").html("");
            if ( type = $("input[name='type']:checked").val() == "department") {
                var id = "department_" + $("#department a.active").attr("node");
                var dept_name = $("#department a.active span").text();
                var email = "部门组";
                var name = dept_name + "&lt;" + email + "&gt;";
                var html_string = conv_address_item(id, name);
                $("#addr_list").html(html_string);
            }
            for (s in result.data) {
                var id = result.data[s].id;
                var realname = result.data[s].realname;
                var username = result.data[s].username;
                var position_name = result.data[s].position_name;
                var name = realname+ "&lt;" + username + "&gt;" + "/" + position_name;
                var html_string = conv_address_item(id, name);
                $("#addr_list").append(html_string);
            }
        }



        // 双击添加到收件人 因后添加的数据 所以需要用live函数
        $(document).on("dblclick", "#addr_list label", function () {
            $text = $(this).text();
            $val = $(this).find("input").val();
            if ($("#rc select option[value='" + $val + "']").val() == undefined) {
                $("<option></option>").val($val).text($text).appendTo("#rc select");
                $("#rc_count").text("(" + $("#rc select option").length + ")");
            }
            ;
        });

        /* 双击添加到收件人 因后添加的数据 所以需要用live函数 */
        $("#rc select").on("dblclick", function () {
            $(this).find("option:selected").remove();
            $("#rc_count").text("(" + $("#rc select option").length + ")");
        });

        $("#cc select").on("dblclick", function () {
            $(this).find("option:selected").remove();
            $("#cc_count").text("(" + $("#cc select option").length + ")");
        });

        $("#bcc select").on("dblclick", function () {
            $(this).find("option:selected").remove();
            $("#bcc_count").text("(" + $("#bcc select option").length + ")");
        });

        $("#addr_list").on("mouseover", function () {
            $("#addr_list label").draggable({
                appendTo: "body",
                helper: "clone"
            });
        });

        $("#rc select").droppable({
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            accept: ":not(.ui-sortable-helper)",
            drop: function (event, ui) {
                $text = ui.draggable.text();
                $val = ui.draggable.find("input").val();
                if ($("#rc select option[value='" + $val + "']").val() == undefined) {
                    $("<option></option>").val($val).text($text).appendTo(this);
                    $("#rc_count").text("(" + $("#rc select option").length + ")");
                }
                ;
            }
        }).sortable({
            items: "option:not(.placeholder)",
            sort: function () {
                // gets added unintentionally by droppable interacting with sortable
                // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
                $(this).removeClass("ui-state-default");
            }
        });

        $("#cc select").droppable({
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            accept: ":not(.ui-sortable-helper)",
            drop: function (event, ui) {
                $text = ui.draggable.text();
                $val = ui.draggable.find("input").val();
                if ($("#cc select option[value='" + $val + "']").val() == undefined) {
                    $("<option></option>").val($val).text($text).appendTo(this);
                    $("#cc_count").text("(" + $("#cc select option").length + ")");
                }
                ;
            },
        }).sortable({
            items: "li:not(.placeholder)",
            sort: function () {
                // gets added unintentionally by droppable interacting with sortable
                // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
                $(this).removeClass("ui-state-default");
            }
        });

        $("#bcc select").droppable({
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            accept: ":not(.ui-sortable-helper)",
            drop: function (event, ui) {
                $text = ui.draggable.text();
                $val = ui.draggable.find("input").val();
                if ($("#bcc select option[value='" + $val + "']").val() == undefined) {
                    $("<option></option>").val($val).text($text).appendTo(this);
                    $("#bcc_count").text("(" + $("#bcc select option").length + ")");
                }
                ;
            },
        }).sortable({
            items: "li:not(.placeholder)",
            sort: function () {
                $(this).removeClass("ui-state-default");
            }
        });
    })

    //最终确认

    function add_address(name) {
        $("input:checked[name='addr_id']").each(function () {
            $text = $(this).parents("label").find("span").text();
            $val = $(this).val();
            if ($("#" + name + " select option[value='" + $val + "']").val() == undefined) {
                $("<option></option>").val($val).text($text).appendTo("#" + name + " select");
                $("#" + name + "_count").text("(" + $("#" + name + " select option").length + ")");
            }
            ;
        })
    }

</script>



