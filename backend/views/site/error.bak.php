<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$this->context->layout = 'error';
?>
<div class="site-error" style="padding:15px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        WEB服务请在处理您的请求时发生错误.
    </p>
    <p>
        请和站点管理员取得联系.谢谢.&nbsp;<a href="<?php echo Yii::$app->homeUrl; ?>">点此返回系统首页</a>
    </p>

</div>
