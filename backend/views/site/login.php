<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<header class="navbar-fixed-top">
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <ul class="top-nav">
                    <li><a href="<?= Yii::$app->urlManager->createUrl('/manual') ?>" class="color1" >使用手册</a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl('/login') ?>" class="color1">员工登录</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>

<div class="login-content">
    <div class="container" style="position:relative;">
        <div class="login-img"><img src="/images/login-img.png"/></div>
        <div class="login-box">
            <div class="login-logo">登录</div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg f-left">员工登录</p>

                <!--<div class="f-right"><a href="javascript:void(0)" id="switch_login">手机动态码登录</a></div>-->
                <div class="clear"></div>

                <div id="login_account">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'loginMode')->hiddenInput(['value' => 'login_account'])->label(false); ?>
                    <div class="form-group has-feedback">
                        <?= $form->field($model, 'username')->textInput(['placeholder' => '电子邮件、手机号'])->label(false) ?>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="footer bg-gray">
                        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn bg-orange btn-block']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>


                <div id="login_sms_code" style="display: none;">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'loginMode')->hiddenInput(['value' => 'login_sms_code'])->label(false); ?>
                    <div class="form-group  login-mobile f-left">
                        <?= $form->field($model, 'mobile')->textInput(['placeholder' => $model->getAttributeLabel('mobile')])->label(false) ?>
                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                    </div>
                    &nbsp;<?= Html::buttonInput(Yii::t('app', 'Get SMS Code'), ['class' => 'btn bg-yellow ', 'name' => 'signup-button', 'id' => 'second']) ?>
                    <div class="clear"></div>
                    <div class="form-group">
                        <?= $form->field($model, 'smsCode')->textInput(['placeholder' => $model->getAttributeLabel('smsCode')])->label(false) ?>
                        <span class="glyphicon glyphicon-mobile form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <?= $form->field($model, 'captcha')->widget(yii\captcha\Captcha::className()
                            , ['captchaAction' => 'site/captcha',
                                'imageOptions' => ['alt' => '点击换图', 'title' => '点击换图', 'style' => 'cursor:pointer']])->label(false); ?>

                    </div>
                    <div class="footer bg-gray">
                        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn bg-blue btn-block']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


