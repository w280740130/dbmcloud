<?php
/**
 * Created by PhpStorm.
 * User: ruzuojun
 * Date: 2015/11/23
 * Time: 17:39
 */

namespace backend\components;
use yii\base\UserException;
use common\models\User;
use common\models\Department;
use common\models\Position;
use common\models\Favorites;
use common\models\FavoritesCategory;
use common\models\EconomicType;
use common\models\IndustryType;
use common\models\SalesMethod;
use common\models\SalesStage;
use common\models\CertType;
use common\models\Customer;
use common\models\CustomerPerson;
use common\models\CustomerMemorialDay;
use common\models\CustomerLevel;
use common\models\CustomerSource;
use common\models\Supplier;
use common\models\SupplierPerson;
use common\models\SupplierLevel;
use common\models\SupplierCert;
use common\models\MeasurementUnit;
use common\models\GoodsCategory;
use common\models\GoodsBrand;
use common\models\Goods;
use Yii;

trait MyTrait {

    public static function getId($model,$uuid) {
        $id=0;
        if(empty($uuid)){
            //echo "uuid is miss.";exit;
        }
        if($model=='User'){
            $item = User::find()->where(['auth_key'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='Department'){
            $item = Department::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='Position'){
            $item = Position::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='Favorites'){
            $item = Favorites::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='FavoritesCategory'){
            $item = FavoritesCategory::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }

        if($model=='EconomicType'){
            $item = EconomicType::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='IndustryType'){
            $item = IndustryType::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='SalesMethod'){
            $item = SalesMethod::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='SalesStage'){
            $item = SalesStage::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='CertType'){
            $item = CertType::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='Customer'){
            $item = Customer::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='CustomerPerson'){
            $item = CustomerPerson::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='CustomerMemorialDay'){
            $item = CustomerMemorialDay::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='CustomerLevel'){
            $item = CustomerLevel::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='CustomerSource'){
            $item = CustomerSource::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='Supplier'){
            $item = Supplier::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='SupplierPerson'){
            $item = SupplierPerson::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='SupplierCert'){
            $item = SupplierCert::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='SupplierLevel'){
            $item = SupplierLevel::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='MeasurementUnit'){
            $item = MeasurementUnit::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='GoodsCategory'){
            $item = GoodsCategory::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='GoodsBrand'){
            $item = GoodsBrand::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        if($model=='Goods'){
            $item = Goods::find()->where(['uuid'=>$uuid,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->id : 0;
        }
        return $id;
    }

    public static function getUuid($model,$id) {
        $uuid=0;
        if($model=='Customer'){
            $item = Customer::find()->where(['id'=>$id,'organization_id'=>Yii::$app->user->identity->organization_id])->one();
            $id = isset($item) ? $item->uuid : 0;
        }
        return $uuid;
    }

    public static function checkUsing($model,$id){
        $msg = Yii::t('app', 'In using,Please delete the associated content.');
        if($model=='FavoritesCategory'){
            $count = Favorites::find()->where(['category_id'=>$id,'organization_id'=>Yii::$app->user->identity->organization_id])->count();
            if($count > 0){
                throw new UserException($msg);
            }
        }

        return true;

    }


}
