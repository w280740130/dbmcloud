<?php
/**
 * Created by PhpStorm.
 * User: ruzuojun
 * Date: 2016/2/22
 * Time: 14:13
 */

namespace backend\components;

use Yii;
use yii\base\Component;

class Settings extends Component{

    public $modelClass = 'backend\models\Settings';

    public $organization_id;

    public $cache = 'cache';
    /**
     * @var string the key used to store settings data in cache
     */
    public $cacheKey = 'ucrm-settings';

    protected $model;

    protected $setting;

    /**
     * Initialize the component
     */
    public function init()
    {
        parent::init();
        $this->model = new $this->modelClass;
        $this->organization_id = Yii::$app->user->identity->organization_id;

    }

    /**
     * @param $name
     * @return mixed
     */
    public function get($name){
        $model = $this->model->find()->where(['organization_id'=>$this->organization_id])->one();
        if($model){
            return $model->$name;
        }
        else {
            return false;
        }
    }


}