<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatus */

$this->title = 'Update ' . 'Mysql Status' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mysql Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mysql-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
