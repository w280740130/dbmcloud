<?php

namespace backend\modules\mysql\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\mysql\models\MysqlStatus;

/**
 * MysqlStatusSearch represents the model behind the search form about `backend\modules\mysql\models\\MysqlStatus`.
 */
class MysqlStatusSearch extends MysqlStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'connect', 'uptime', 'max_connections', 'max_connect_errors', 'open_files_limit', 'open_files', 'table_open_cache', 'open_tables', 'max_tmp_tables', 'max_heap_table_size', 'max_allowed_packet', 'threads_connected', 'threads_running', 'threads_waits', 'threads_created', 'threads_cached', 'connections', 'aborted_clients', 'aborted_connects', 'connections_persecond', 'bytes_received_persecond', 'bytes_sent_persecond', 'com_select_persecond', 'com_insert_persecond', 'com_update_persecond', 'com_delete_persecond', 'com_commit_persecond', 'com_rollback_persecond', 'questions_persecond', 'queries_persecond', 'transaction_persecond', 'created_tmp_tables_persecond', 'created_tmp_disk_tables_persecond', 'created_tmp_files_persecond', 'table_locks_immediate_persecond', 'table_locks_waited_persecond', 'key_buffer_size', 'sort_buffer_size', 'join_buffer_size', 'key_blocks_not_flushed', 'key_blocks_unused', 'key_blocks_used', 'key_read_requests_persecond', 'key_reads_persecond', 'key_write_requests_persecond', 'key_writes_persecond', 'innodb_buffer_pool_instances', 'innodb_buffer_pool_size', 'innodb_flush_log_at_trx_commit', 'innodb_force_recovery', 'innodb_io_capacity', 'innodb_read_io_threads', 'innodb_write_io_threads', 'innodb_buffer_pool_pages_total', 'innodb_buffer_pool_pages_data', 'innodb_buffer_pool_pages_dirty', 'innodb_buffer_pool_pages_flushed', 'innodb_buffer_pool_pages_free', 'innodb_buffer_pool_pages_misc', 'innodb_page_size', 'innodb_pages_created', 'innodb_pages_read', 'innodb_pages_written', 'innodb_buffer_pool_pages_flushed_persecond', 'innodb_buffer_pool_read_requests_persecond', 'innodb_buffer_pool_reads_persecond', 'innodb_buffer_pool_write_requests_persecond', 'innodb_rows_read_persecond', 'innodb_rows_inserted_persecond', 'innodb_rows_updated_persecond', 'innodb_rows_deleted_persecond'], 'integer'],
            [['host', 'port', 'tags', 'role', 'version', 'innodb_version', 'innodb_doublewrite', 'innodb_file_per_table', 'innodb_flush_method', 'innodb_row_lock_current_waits', 'query_cache_hitrate', 'thread_cache_hitrate', 'key_buffer_read_rate', 'key_buffer_write_rate', 'key_blocks_used_rate', 'created_tmp_disk_tables_rate', 'connections_usage_rate', 'open_files_usage_rate', 'open_tables_usage_rate', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MysqlStatus::find();
        
        $query->orderBy(['connect' => SORT_ASC,'tags'=>SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params) && !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'connect' => $this->connect,
            'uptime' => $this->uptime,
            'max_connections' => $this->max_connections,
            'max_connect_errors' => $this->max_connect_errors,
            'open_files_limit' => $this->open_files_limit,
            'open_files' => $this->open_files,
            'table_open_cache' => $this->table_open_cache,
            'open_tables' => $this->open_tables,
            'max_tmp_tables' => $this->max_tmp_tables,
            'max_heap_table_size' => $this->max_heap_table_size,
            'max_allowed_packet' => $this->max_allowed_packet,
            'threads_connected' => $this->threads_connected,
            'threads_running' => $this->threads_running,
            'threads_waits' => $this->threads_waits,
            'threads_created' => $this->threads_created,
            'threads_cached' => $this->threads_cached,
            'connections' => $this->connections,
            'aborted_clients' => $this->aborted_clients,
            'aborted_connects' => $this->aborted_connects,
            'connections_persecond' => $this->connections_persecond,
            'bytes_received_persecond' => $this->bytes_received_persecond,
            'bytes_sent_persecond' => $this->bytes_sent_persecond,
            'com_select_persecond' => $this->com_select_persecond,
            'com_insert_persecond' => $this->com_insert_persecond,
            'com_update_persecond' => $this->com_update_persecond,
            'com_delete_persecond' => $this->com_delete_persecond,
            'com_commit_persecond' => $this->com_commit_persecond,
            'com_rollback_persecond' => $this->com_rollback_persecond,
            'questions_persecond' => $this->questions_persecond,
            'queries_persecond' => $this->queries_persecond,
            'transaction_persecond' => $this->transaction_persecond,
            'created_tmp_tables_persecond' => $this->created_tmp_tables_persecond,
            'created_tmp_disk_tables_persecond' => $this->created_tmp_disk_tables_persecond,
            'created_tmp_files_persecond' => $this->created_tmp_files_persecond,
            'table_locks_immediate_persecond' => $this->table_locks_immediate_persecond,
            'table_locks_waited_persecond' => $this->table_locks_waited_persecond,
            'key_buffer_size' => $this->key_buffer_size,
            'sort_buffer_size' => $this->sort_buffer_size,
            'join_buffer_size' => $this->join_buffer_size,
            'key_blocks_not_flushed' => $this->key_blocks_not_flushed,
            'key_blocks_unused' => $this->key_blocks_unused,
            'key_blocks_used' => $this->key_blocks_used,
            'key_read_requests_persecond' => $this->key_read_requests_persecond,
            'key_reads_persecond' => $this->key_reads_persecond,
            'key_write_requests_persecond' => $this->key_write_requests_persecond,
            'key_writes_persecond' => $this->key_writes_persecond,
            'innodb_buffer_pool_instances' => $this->innodb_buffer_pool_instances,
            'innodb_buffer_pool_size' => $this->innodb_buffer_pool_size,
            'innodb_flush_log_at_trx_commit' => $this->innodb_flush_log_at_trx_commit,
            'innodb_force_recovery' => $this->innodb_force_recovery,
            'innodb_io_capacity' => $this->innodb_io_capacity,
            'innodb_read_io_threads' => $this->innodb_read_io_threads,
            'innodb_write_io_threads' => $this->innodb_write_io_threads,
            'innodb_buffer_pool_pages_total' => $this->innodb_buffer_pool_pages_total,
            'innodb_buffer_pool_pages_data' => $this->innodb_buffer_pool_pages_data,
            'innodb_buffer_pool_pages_dirty' => $this->innodb_buffer_pool_pages_dirty,
            'innodb_buffer_pool_pages_flushed' => $this->innodb_buffer_pool_pages_flushed,
            'innodb_buffer_pool_pages_free' => $this->innodb_buffer_pool_pages_free,
            'innodb_buffer_pool_pages_misc' => $this->innodb_buffer_pool_pages_misc,
            'innodb_page_size' => $this->innodb_page_size,
            'innodb_pages_created' => $this->innodb_pages_created,
            'innodb_pages_read' => $this->innodb_pages_read,
            'innodb_pages_written' => $this->innodb_pages_written,
            'innodb_buffer_pool_pages_flushed_persecond' => $this->innodb_buffer_pool_pages_flushed_persecond,
            'innodb_buffer_pool_read_requests_persecond' => $this->innodb_buffer_pool_read_requests_persecond,
            'innodb_buffer_pool_reads_persecond' => $this->innodb_buffer_pool_reads_persecond,
            'innodb_buffer_pool_write_requests_persecond' => $this->innodb_buffer_pool_write_requests_persecond,
            'innodb_rows_read_persecond' => $this->innodb_rows_read_persecond,
            'innodb_rows_inserted_persecond' => $this->innodb_rows_inserted_persecond,
            'innodb_rows_updated_persecond' => $this->innodb_rows_updated_persecond,
            'innodb_rows_deleted_persecond' => $this->innodb_rows_deleted_persecond,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'innodb_version', $this->innodb_version])
            ->andFilterWhere(['like', 'innodb_doublewrite', $this->innodb_doublewrite])
            ->andFilterWhere(['like', 'innodb_file_per_table', $this->innodb_file_per_table])
            ->andFilterWhere(['like', 'innodb_flush_method', $this->innodb_flush_method])
            ->andFilterWhere(['like', 'innodb_row_lock_current_waits', $this->innodb_row_lock_current_waits])
            ->andFilterWhere(['like', 'query_cache_hitrate', $this->query_cache_hitrate])
            ->andFilterWhere(['like', 'thread_cache_hitrate', $this->thread_cache_hitrate])
            ->andFilterWhere(['like', 'key_buffer_read_rate', $this->key_buffer_read_rate])
            ->andFilterWhere(['like', 'key_buffer_write_rate', $this->key_buffer_write_rate])
            ->andFilterWhere(['like', 'key_blocks_used_rate', $this->key_blocks_used_rate])
            ->andFilterWhere(['like', 'created_tmp_disk_tables_rate', $this->created_tmp_disk_tables_rate])
            ->andFilterWhere(['like', 'connections_usage_rate', $this->connections_usage_rate])
            ->andFilterWhere(['like', 'open_files_usage_rate', $this->open_files_usage_rate])
            ->andFilterWhere(['like', 'open_tables_usage_rate', $this->open_tables_usage_rate]);

        return $dataProvider;
    }
}
