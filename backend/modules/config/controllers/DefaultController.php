<?php

namespace backend\modules\config\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->redirect(['oracle/index']);
        //return $this->render('index');
    }
}
