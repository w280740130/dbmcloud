<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\MongodbServerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mongodb-server-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'port') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'tags') ?>

    <?php // echo $form->field($model, 'monitor') ?>

    <?php // echo $form->field($model, 'send_mail') ?>

    <?php // echo $form->field($model, 'send_mail_to_list') ?>

    <?php // echo $form->field($model, 'send_sms') ?>

    <?php // echo $form->field($model, 'send_sms_to_list') ?>

    <?php // echo $form->field($model, 'alarm_connections_current') ?>

    <?php // echo $form->field($model, 'alarm_active_clients') ?>

    <?php // echo $form->field($model, 'alarm_current_queue') ?>

    <?php // echo $form->field($model, 'threshold_connections_current') ?>

    <?php // echo $form->field($model, 'threshold_active_clients') ?>

    <?php // echo $form->field($model, 'threshold_current_queue') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
