<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OracleServerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="oracle-server-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'port') ?>

    <?= $form->field($model, 'dsn') ?>

    <?= $form->field($model, 'username') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'tags') ?>

    <?php // echo $form->field($model, 'monitor') ?>

    <?php // echo $form->field($model, 'send_mail') ?>

    <?php // echo $form->field($model, 'send_mail_to_list') ?>

    <?php // echo $form->field($model, 'send_sms') ?>

    <?php // echo $form->field($model, 'send_sms_to_list') ?>

    <?php // echo $form->field($model, 'alarm_session_total') ?>

    <?php // echo $form->field($model, 'alarm_session_actives') ?>

    <?php // echo $form->field($model, 'alarm_session_waits') ?>

    <?php // echo $form->field($model, 'alarm_tablespace') ?>

    <?php // echo $form->field($model, 'threshold_session_total') ?>

    <?php // echo $form->field($model, 'threshold_session_actives') ?>

    <?php // echo $form->field($model, 'threshold_session_waits') ?>

    <?php // echo $form->field($model, 'threshold_tablespace') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
