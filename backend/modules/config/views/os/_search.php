<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OsServerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="os-server-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'community') ?>

    <?= $form->field($model, 'tags') ?>

    <?= $form->field($model, 'monitor') ?>

    <?php // echo $form->field($model, 'send_mail') ?>

    <?php // echo $form->field($model, 'send_mail_to_list') ?>

    <?php // echo $form->field($model, 'send_sms') ?>

    <?php // echo $form->field($model, 'send_sms_to_list') ?>

    <?php // echo $form->field($model, 'alarm_os_process') ?>

    <?php // echo $form->field($model, 'alarm_os_load') ?>

    <?php // echo $form->field($model, 'alarm_os_cpu') ?>

    <?php // echo $form->field($model, 'alarm_os_network') ?>

    <?php // echo $form->field($model, 'alarm_os_disk') ?>

    <?php // echo $form->field($model, 'alarm_os_memory') ?>

    <?php // echo $form->field($model, 'threshold_os_process') ?>

    <?php // echo $form->field($model, 'threshold_os_load') ?>

    <?php // echo $form->field($model, 'threshold_os_cpu') ?>

    <?php // echo $form->field($model, 'threshold_os_network') ?>

    <?php // echo $form->field($model, 'threshold_os_disk') ?>

    <?php // echo $form->field($model, 'threshold_os_memory') ?>

    <?php // echo $form->field($model, 'filter_os_disk') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
