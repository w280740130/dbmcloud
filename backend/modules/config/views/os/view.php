<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OsServer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Os Servers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="os-server-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'host',
            'community',
            'tags',
            'monitor',
            'send_mail',
            'send_mail_to_list',
            'send_sms',
            'send_sms_to_list',
            'alarm_os_process',
            'alarm_os_load',
            'alarm_os_cpu',
            'alarm_os_network',
            'alarm_os_disk',
            'alarm_os_memory',
            'threshold_os_process',
            'threshold_os_load',
            'threshold_os_cpu',
            'threshold_os_network',
            'threshold_os_disk',
            'threshold_os_memory',
            'filter_os_disk',
            'create_time',
        ],
    ]) ?>

</div>
