<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\MysqlServer */

$this->title = '更新MySQL实例: ' . ' ' . $model->tags;
$this->params['breadcrumbs'][] = ['label' => 'MySQL实例', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tags, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '更新';
?>
<div class="mysql-server-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
