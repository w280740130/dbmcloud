<?php

namespace backend\modules\config\models;

use Yii;

/**
 * This is the model class for table "mysql_server".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $username
 * @property string $password
 * @property string $tags
 * @property integer $monitor
 * @property integer $send_mail
 * @property string $send_mail_to_list
 * @property integer $send_sms
 * @property string $send_sms_to_list
 * @property string $send_slowquery_to_list
 * @property integer $alarm_threads_connected
 * @property integer $alarm_threads_running
 * @property integer $alarm_threads_waits
 * @property integer $alarm_repl_status
 * @property integer $alarm_repl_delay
 * @property integer $threshold_threads_connected
 * @property integer $threshold_threads_running
 * @property integer $threshold_threads_waits
 * @property integer $threshold_repl_delay
 * @property integer $slow_query
 * @property integer $bigtable_monitor
 * @property integer $bigtable_size
 * @property integer $created_at
 * @property integer $updated_at
 */
class MysqlServer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mysql_server';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host', 'port','username', 'password','tags'], 'required'],
            [['port','monitor', 'send_mail', 'send_sms', 'alarm_threads_connected', 'alarm_threads_running', 'alarm_threads_waits', 'alarm_repl_status', 'alarm_repl_delay', 'threshold_threads_connected', 'threshold_threads_running', 'threshold_threads_waits', 'threshold_repl_delay', 'slow_query', 'bigtable_monitor', 'bigtable_size', 'created_at', 'updated_at'], 'integer'],
            [['host', 'username', 'password'], 'string', 'max' => 30],
            [['port'], 'string', 'max' => 10],
            [['tags'], 'string', 'max' => 50],
            [['send_mail_to_list', 'send_sms_to_list', 'send_slowquery_to_list'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => '主机',
            'port' => '端口',
            'username' => '用户',
            'password' => '密码',
            'tags' => '标签',
            'monitor' => '监控',
            'send_mail' => '邮件通知',
            'send_mail_to_list' => '邮件收件人列表',
            'send_sms' => '发送短信',
            'send_sms_to_list' => '短信收件人列表',
            'send_slowquery_to_list' => '慢查询邮件接收人',
            'alarm_threads_connected' => '连接线程数告警',
            'alarm_threads_running' => '运行线程数告警',
            'alarm_threads_waits' => '等待线程数告警',
            'alarm_repl_status' => '同步状态告警',
            'alarm_repl_delay' => '同步延迟告警',
            'threshold_threads_connected' => '连接线程数阀值',
            'threshold_threads_running' => '运行线程数阀值',
            'threshold_threads_waits' => '等待线程数阀值',
            'threshold_repl_delay' => '同步延迟阀值',
            'slow_query' => 'slow_query',
            'bigtable_monitor' => 'bigtable_monitor',
            'bigtable_size' => 'bigtable_size',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    /**
     * Before save.
     *
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            if($insert) {

            }
            return true;
        }
        else
            return false;
    }
}
