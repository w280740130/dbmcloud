<?php

namespace backend\modules\config\models;

use Yii;

/**
 * This is the model class for table "mongodb_server".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $username
 * @property string $password
 * @property string $tags
 * @property integer $monitor
 * @property integer $send_mail
 * @property string $send_mail_to_list
 * @property integer $send_sms
 * @property string $send_sms_to_list
 * @property integer $alarm_connections_current
 * @property integer $alarm_active_clients
 * @property integer $alarm_current_queue
 * @property integer $threshold_connections_current
 * @property integer $threshold_active_clients
 * @property integer $threshold_current_queue
 * @property string $create_time
 */
class MongodbServer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mongodb_server';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host', 'port','username', 'password','tags'], 'required'],
            [['monitor', 'send_mail', 'send_sms', 'alarm_connections_current', 'alarm_active_clients', 'alarm_current_queue', 'threshold_connections_current', 'threshold_active_clients', 'threshold_current_queue'], 'integer'],
            [['create_time'], 'safe'],
            [['host'], 'string', 'max' => 30],
            [['port'], 'string', 'max' => 10],
            [['username', 'password', 'tags'], 'string', 'max' => 50],
            [['send_mail_to_list', 'send_sms_to_list'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => '主机',
            'port' => '端口',
            'username' => '用户',
            'password' => '密码',
            'tags' => '标签',
            'monitor' => '监控',
            'send_mail' => '邮件通知',
            'send_mail_to_list' => '邮件收件人列表',
            'send_sms' => '发送短信',
            'send_sms_to_list' => '短信收件人列表',
            'alarm_connections_current' => 'Connections Current',
            'alarm_active_clients' => 'Active Clients',
            'alarm_current_queue' => 'Current Queue',
            'threshold_connections_current' => 'Connections Current',
            'threshold_active_clients' => 'Active Clients',
            'threshold_current_queue' => 'Current Queue',
            'create_time' => 'Create Time',
        ];
    }
}
