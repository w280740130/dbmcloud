<?php

namespace backend\modules\sys\controllers;

use backend\models\Menu;
use common\models\User;
use Yii;
use common\models\Base;
use common\models\Department;
use common\models\DepartmentSearch;
use yii\web\Controller;
use backend\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;


/**
 * DepartmentController implements the CRUD actions for Department model.
 */
class DepartmentController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }


    /**
     * Lists all Department models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new DepartmentSearch();
        $dataProvider = Department::getTree(0, Department::getAll());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionTree(){
        //$dataProvider = Department::get(0, Department::find()->asArray()->all());
        //echo json_encode($dataProvider);
        $array = array('id'=>1,'name'=>'aaaa');
        echo Json::encode($array);

    }

    /**
     * Displays a single Department model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($uuid)
    {
        $id = Yii::$app->system->getId(Department::className(),$uuid);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Department model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Department();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $post = Yii::$app->request->post();
            $parent_id = isset($post['Department']['parent_id']) ? $post['Department']['parent_id'] : 0;

            if($parent_id!=0){
                $parent = $model::find()->select(['id','parent_id','name','level'])->where(['id'=>$parent_id])->one();
                $level = $parent->level;
                $model->level = $level+1;
            }
            else{
                $model->level = 1;
                $model->parent_id = 0;
            }
            $model->save();
            Yii::$app->system->writeLog();
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Department model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($uuid)
    {

        $id = Yii::$app->system->getId(Department::className(),$uuid);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $post = Yii::$app->request->post();
            $parent_id = isset($post['Department']['parent_id']) ? $post['Department']['parent_id'] : 0;

            if($parent_id!=0){
                $parent = $model::find()->select(['id','parent_id','name','level'])->where(['id'=>$parent_id])->one();
                $level = $parent->level;
                $model->level = $level+1;
            }
            else{
                $model->level = 1;
                $model->parent_id = 0;
            }
            $model->save();
            Yii::$app->system->writeLog();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Department model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($uuid)
    {

        $id = Yii::$app->system->getId(Department::className(),$uuid);
        Yii::$app->system->checkHasChild(Department::className(),['parent_id'=>$id]);
        Yii::$app->system->checkHasChild(User::className(),['department'=>$id]);
        $this->findModel($id)->delete();
        Yii::$app->system->writeLog();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Department model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Department the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Department::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
