<?php

namespace backend\modules\sys\controllers;

use yii;
use yii\web\Controller;
use common\models\Organization;

class DefaultController extends BaseController
{
    public function actionIndex()
    {
        $model = Organization::find()->where(['id'=>Yii::$app->user->identity->organization_id])->one();
        return $this->render('index',['model'=>$model]);
    }
}
