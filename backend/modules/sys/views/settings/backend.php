<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Organization;
use backend\models\Settings;
use backend\models\Module;

/* @var $this yii\web\View */
/* @var $model common\models\Organization */
/* @var $form ActiveForm */

$this->title = '后台设置';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="settings-index">
    <?php $form = ActiveForm::begin(); ?>


    <div class="form-group">
        <?= $form->field($model, 'backend_theme')->dropDownList(Settings::getBackendTheme()) ?>
        <?= $form->field($model, 'enable_modules')->checkboxList(ArrayHelper::map(Module::getNoSysModules(),'id','title') ) ?>
        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div><!-- organization-index -->
