<?php

$this->title = Yii::t('app', 'Update ') . Yii::t('app', '{name}', ['name' => $model->description]);
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::t('app', 'Roles'),
        'url' => ['/role']
    ],
    $this->title
];

echo $this->render('_form', [
    'model' => $model,
	'role' => $role,
    'permissions' => $permissions,
]);