<?php

namespace backend\modules\sys;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\sys\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
