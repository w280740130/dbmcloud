<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\alarm\models\AlarmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alarm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tags') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'port') ?>

    <?= $form->field($model, 'create_time') ?>

    <?php // echo $form->field($model, 'db_type') ?>

    <?php // echo $form->field($model, 'alarm_item') ?>

    <?php // echo $form->field($model, 'alarm_value') ?>

    <?php // echo $form->field($model, 'level') ?>

    <?php // echo $form->field($model, 'message') ?>

    <?php // echo $form->field($model, 'detail') ?>

    <?php // echo $form->field($model, 'send_mail') ?>

    <?php // echo $form->field($model, 'send_sms') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
