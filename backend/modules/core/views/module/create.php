<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Module */

$this->title = Yii::t('app', 'Create ') . Yii::t('app', 'Module');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
