<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ModuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Modules');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-index">

    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> 温馨提示!</h4>
        模块项目非专业开发人员请勿修改和删除.否则可能引起系统访问异常.
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Create ') . Yii::t('app', 'Module'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'title',
            'description',
            [
                'attribute' => 'private',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->private==1 ? Yii::t('app','YES') : Yii::t('app','NO');
                    },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'private',
                    \common\models\Base::getYnStatus(),
                    ['class' => 'form-control customer-select', 'prompt' => Yii::t('app', 'All')]
                ),
                'headerOptions' => ['width' => '100'],

            ],
            [
                'attribute' => 'system',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->system==1 ? Yii::t('app','YES') : Yii::t('app','NO');
                    },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'system',
                    \common\models\Base::getYnStatus(),
                    ['class' => 'form-control customer-select', 'prompt' => Yii::t('app', 'All')]
                ),
                'headerOptions' => ['width' => '100'],

            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->status==1 ? Yii::t('app','STATUS_ON_SHELF') : Yii::t('app','STATUS_OFF_SHELF');
                    },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    \common\models\Base::getShelfStatus(),
                    ['class' => 'form-control customer-select', 'prompt' => Yii::t('app', 'All')]
                ),
                'headerOptions' => ['width' => '110'],

            ],

            [
                'attribute' => 'sort_order',
                'headerOptions' => ['width' => '65'],
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
