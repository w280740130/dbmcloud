<?php

namespace backend\modules\oracle\models;

use Yii;

/**
 * This is the model class for table "oracle_dba_indexes".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $tags
 * @property string $owner
 * @property string $index_name
 * @property string $index_type
 * @property string $table_owner
 * @property string $table_name
 * @property string $status
 * @property string $create_time
 */
class OracleIndexes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oracle_dba_indexes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner', 'index_type', 'table_owner'], 'required'],
            [['create_time'], 'safe'],
            [['host', 'tags'], 'string', 'max' => 50],
            [['port'], 'string', 'max' => 30],
            [['owner', 'index_name', 'index_type', 'table_owner', 'table_name', 'status'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => '主机',
            'port' => '端口',
            'tags' => '标签',
            'owner' => '隶属用户',
            'index_name' => '索引名称',
            'index_type' => '索引类型',
            'table_owner' => '表Owner',
            'table_name' => '表名称',
            'status' => '状态',
            'distinct_keys' => '唯一值',
            'num_rows' => '总行数',
            'blevel' => '二元高度',
            'create_time' => '采集时间',
        ];
    }
}
