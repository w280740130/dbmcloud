<?php

namespace backend\modules\oracle\models;

use Yii;

/**
 * This is the model class for table "oracle_tablespace".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $tags
 * @property string $tablespace_name
 * @property string $total_size
 * @property string $used_size
 * @property string $avail_size
 * @property string $used_rate
 * @property string $create_time
 */
class OracleTablespace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oracle_tablespace';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tablespace_name'], 'required'],
            [['total_size', 'used_size', 'avail_size'], 'integer'],
            [['create_time'], 'safe'],
            [['host', 'tags'], 'string', 'max' => 50],
            [['port'], 'string', 'max' => 30],
            [['tablespace_name'], 'string', 'max' => 100],
            [['used_rate'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => '主机',
            'port' => '端口',
            'tags' => '标签',
            'tablespace_name' => '表空间',
            'total_size' => '总大小',
            'used_size' => '已使用',
            'avail_size' => '可用',
            'used_rate' => '使用率',
            'create_time' => '采集时间',
        ];
    }
}
