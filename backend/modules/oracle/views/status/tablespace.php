<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use backend\helpers\Tools;
/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleStatus */

$this->title = $model->host . ':' . $model->port . ' [' . $model->tags .':'.$model->database_role. ']';
$this->params['breadcrumbs'][] = ['label' => 'Oracle 性能监控平台', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host . ':' . $model->port;
?>
<div class="oracle-status-view">

    <?php echo $this->render('_tab', ['model' => $model]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'host',
            'port',
            'tags',
            'tablespace_name',
            [
                'attribute' => 'total_size',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::formatMbytes($model->total_size);
                    },

            ],
            [
                'attribute' => 'used_size',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::formatMbytes($model->used_size);
                    },

            ],
            [
                'attribute' => 'avail_size',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::formatMbytes($model->avail_size);
                    },

            ],
            [
                'attribute' => 'used_rate',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return ($model->used_rate >= 85) ? "<span class='btn btn-xs bg-orange btn-flat'>$model->used_rate%</span>" : "<span class='btn btn-xs bg-green btn-flat'>$model->used_rate%</span>";
                    },

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Operate'),
                'headerOptions' => ['width' => '75'],
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',  Yii::$app->urlManager->createUrl(['oracle/tablespace/chart','server'=>$model->host.'--'.$model->port,'time_interval'=>'3600','end'=>'0']), ['title' => '查看监控详情' ]);
                    },

                ],

            ],
        ],
    ]); ?>


</div>
