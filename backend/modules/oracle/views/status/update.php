<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleStatus */

$this->title = 'Update Oracle Status: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Oracle Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="oracle-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
