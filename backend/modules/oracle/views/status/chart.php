<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\widgets\DetailView;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatus */

$this->title = $model->host . ':' . $model->port . ' [' . $model->tags .':'.$model->database_role. ']';
$this->params['breadcrumbs'][] = ['label' => 'Oracle 性能监控平台', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host.':'.$model->port;
?>

<?php
$data = $model->find()->orderBy(['id' => SORT_DESC, 'host' => SORT_ASC])->all();
echo $this->render('_tab',['model'=>$model]);
?>

<div class="container-fluid mt15">
    <div class="col-xs-5 text-left">
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/status/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'3600','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==3600): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">1小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/status/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'21600','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==21600): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">6小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/status/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'43200','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==43200): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">12小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/status/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'86400','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==86400): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">1天</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/status/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'259200','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==259200): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">3天</a>
    </div>
    <?php $form = ActiveForm::begin(['action'=>['status/chart'],'method'=>'post']); ?>
    <div class="col-xs-5 text-left">

        <?= DateRangePicker::widget([
            'name' => 'time_from',
            'value' => $time_from,
            'nameTo' => 'time_to',
            'valueTo' => $time_to,
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-m-d'
            ]
        ]);?>

    </div>
    <div class="col-xs-1 text-left">
        <?= Html::activeHiddenInput($model,'host'); ?>
        <?= Html::activeHiddenInput($model,'port'); ?>
        <?= Html::submitButton('生成图表', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<hr/>
<div class="container-fluid mt15" style="line-height: 30px;">

    <a href="#main_chart_connect" class="btn btn-warning btn-xs">Connectivity</a>
    <a href="#main_chart_session_total" class="btn btn-warning btn-xs">Session Total</a>
    <a href="#main_chart_session_actives" class="btn btn-warning btn-xs">Session Actives</a>
    <a href="#main_chart_session_waits" class="btn btn-warning btn-xs">Sessions Watis</a>
    <a href="#main_chart_parse_count_inc" class="btn btn-warning btn-xs">Parse Count Inc</a>
    <a href="#main_chart_invalid_objects" class="btn btn-warning btn-xs">Invalid Objects</a>
    <a href="#main_chart_invalid_indexes" class="btn btn-warning btn-xs">Invalid Indexes</a>
    <a href="#main_chart_sqlmonitor" class="btn btn-warning btn-xs">SQLMonitor Lag</a>
    <a href="#main_chart_rollstat" class="btn btn-warning btn-xs">Rollstat Active</a>
    <a href="#main_chart_tempsql" class="btn btn-warning btn-xs">Temp SQL</a>
    <a href="#main_chart_data_dict" class="btn btn-warning btn-xs">Data Dict Rate</a>
    <a href="#main_chart_buffer_pool" class="btn btn-warning btn-xs">Buffer Pool Rate</a>
    <a href="#main_chart_physical_wr" class="btn btn-warning btn-xs">Physical W/R</a>
    <a href="#main_chart_load" class="btn btn-warning btn-xs">CPU Load</a>
    <a href="#main_chart_cpu" class="btn btn-warning btn-xs">CPU Time</a>


    <hr/>

    <div id="main_chart_connect" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_session_total" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_session_actives" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_session_waits" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_parse_count_inc" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_invalid_objects" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_invalid_indexes" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_sqlmonitor" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_rollstat" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_tempsql" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_data_dict" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_buffer_pool" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_physical_wr" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_cpu" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_load" class="col-xs-12 text-center line-chart"></div>
</div>



<?php \common\widgets\JsBlock::begin() ?>

<script type="text/javascript">
    //Connectivity
    var myChart = echarts.init(document.getElementById('main_chart_connect'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#66CC99'],
        title : {
            text: 'Connectivity',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Connectivity'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Connectivity',
                type:'line',
                smooth:true,
                itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['connect'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //Sessions Total
    var myChart = echarts.init(document.getElementById('main_chart_session_total'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333','#FF9966'],
        title : {
            text: 'Session Total',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Session Total','Alarm Threshold','Processes Max'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [

            {
                name:'Session Total',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['session_total'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_session_total'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Processes Max',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['processes'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            }

        ]
    });

</script>


<script type="text/javascript">
    //Session Actives
    var myChart = echarts.init(document.getElementById('main_chart_session_actives'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333'],
        title : {
            text: 'Session Actives',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Session Actives','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Session Actives',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['session_actives'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
                /*
                markPoint: {
                    data: [
                        {type: 'max', name: '最大值'},
                        {type: 'min', name: '最小值'}
                    ]
                },
                */
                markLine: {
                    data: [
                        {type: 'average', name: '平均值'}
                    ]
                }
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_session_actives'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>


<script type="text/javascript">
    //Session Waits
    var myChart = echarts.init(document.getElementById('main_chart_session_waits'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333'],
        title : {
            text: 'Session Waits',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Session Waits','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Session Waits',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['session_waits'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
                /*
                 markPoint: {
                 data: [
                 {type: 'max', name: '最大值'},
                 {type: 'min', name: '最小值'}
                 ]
                 },
                 */
                markLine: {
                    data: [
                        {type: 'average', name: '平均值'}
                    ]
                }
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_session_waits'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>




<script type="text/javascript">
    //Physicam WR
    var myChart = echarts.init(document.getElementById('main_chart_physical_wr'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#66CC66','#FF9966'],
        title : {
            text: 'Physical W/R',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Physical Read','Physical Write'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Physical Read',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['physical_reads_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Physical Write',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['physical_writes_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //parse count inc
    var myChart = echarts.init(document.getElementById('main_chart_parse_count_inc'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#66CC66','#FF9966'],
        title : {
            text: 'Parse Count Inc',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Parse Count Total Inc','Parse Count Hard Inc'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Parse Count Total Inc',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['parse_count_total_inc'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
                markLine: {
                    data: [
                        {type: 'average', name: '平均值'}
                    ]
                }
            },
            {
                name:'Parse Count Hard Inc',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['parse_count_hard_inc'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
                markLine: {
                    data: [
                        {type: 'average', name: '平均值'}
                    ]
                }
            },

        ]
    });

</script>

<script type="text/javascript">
    //Invalid Objects
    var myChart = echarts.init(document.getElementById('main_chart_invalid_objects'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333'],
        title : {
            text: 'Invalid Objects Count',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Invalid Objects','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Invalid Objects',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['objects_invalid_count'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_objects'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //Invalid indexes
    var myChart = echarts.init(document.getElementById('main_chart_invalid_indexes'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333'],
        title : {
            text: 'Invalid Indexes Count',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Invalid Indexes','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Invalid Indexes',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['indexes_invalid_count'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_index'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>


<script type="text/javascript">
    //sqlmonitor
    var myChart = echarts.init(document.getElementById('main_chart_sqlmonitor'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333'],
        title : {
            text: 'SQL Monitor lag',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['SQL Monitor lag','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'SQL Monitor lag',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['sqlmonitor_count'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_sqlmonitor'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>


<script type="text/javascript">
    //rollstat
    var myChart = echarts.init(document.getElementById('main_chart_rollstat'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333'],
        title : {
            text: 'Rollstat Active',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['SQL Monitor lag','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Rollstat Active',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['rollstat_segs_count'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_rollstat'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>



<script type="text/javascript">
    //tempsql
    var myChart = echarts.init(document.getElementById('main_chart_tempsql'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333'],
        title : {
            text: 'Temp SQL',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Temp SQL','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Temp SQL',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['temp_tbs_sql_count'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_temp_tbs'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>


<script type="text/javascript">
    //data dict
    var myChart = echarts.init(document.getElementById('main_chart_data_dict'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333'],
        title : {
            text: 'Data Dict Rate',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Data Dict Rate','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Data Dict Rate',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['data_dict_rate'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_data_dict_rate'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>



<script type="text/javascript">
    //buffer pool
    var myChart = echarts.init(document.getElementById('main_chart_buffer_pool'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333'],
        title : {
            text: 'Buffer Cache Rate',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Buffer Pool Rate','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Buffer Pool Rate',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['buffer_pool_rate'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_buffer_pool_rate'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //load
    var myChart = echarts.init(document.getElementById('main_chart_load'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333','#FF9966'],
        title : {
            text: 'CPU Load',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['CPU Load','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($os_chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'CPU Load',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($os_chart_data as $item):
                            $data_array[] = $item['load_1'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
                markLine: {
                    data: [
                        {type: 'average', name: '平均值'}
                    ]
                }
            },

            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($os_chart_data as $item):
                            $data_array[] = $item->servers['threshold_os_load'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
        ]
    });

</script>

<script type="text/javascript">
    //cpu
    var myChart = echarts.init(document.getElementById('main_chart_cpu'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333','#FF9966'],
        title : {
            text: 'CPU Utilization',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['CPU Time Usage','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($os_chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value',
                max:'100',
                axisLabel: {
                    show: true,
                    interval: 'auto',
                    formatter: '{value}%'
                }
            }
        ],
        series : [
            {
                name:'CPU Time Usage',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($os_chart_data as $item):
                            $data_array[] = $item['cpu_user_time']+$item['cpu_system_time'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
                markLine: {
                    data: [
                        {type: 'average', name: '平均值'}
                    ]
                }
            },

            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($os_chart_data as $item):
                            $data_array[] = $item->servers['threshold_os_cpu'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
        ]
    });

</script>

<?php \common\widgets\JsBlock::end()?>


