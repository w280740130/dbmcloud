<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleTablespace */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="oracle-tablespace-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'port')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tablespace_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'used_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'avail_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'used_rate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'create_time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
