<?php

namespace backend\modules\oracle\controllers;

use backend\modules\oracle\models\OracleIndexesSearch;
use backend\modules\oracle\models\OracleObjectsSearch;
use Yii;
use backend\modules\oracle\models\OracleStatus;
use backend\modules\oracle\models\OracleStatusSearch;
use backend\modules\oracle\models\OracleStatusHistory;
use backend\modules\oracle\models\OracleTablespaceSearch;
use backend\modules\os\models\OsStatusHistory;
use backend\modules\alarm\models\Alarm;
use backend\modules\alarm\models\AlarmSearch;
use backend\modules\oracle\models\OracleWaits;
use backend\modules\oracle\models\OracleWaitsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StatusController implements the CRUD actions for OracleStatus model.
 */
class StatusController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all OracleStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OracleStatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OracleStatus model.
     * @param char $server
     * @return mixed
     */
    public function actionView($server)
    {
        $server_array = explode('-',$server);
        $host = $server_array[0];
        $port = $server_array[1];

        $time_interval = 1800;
        $end = 0;
        $start_time = date('YmdHi',(time()-$end-$time_interval));
        $end_time = date('YmdHi',(time()-$end));
        $chart_data = OracleStatusHistory::find()->where((['host' => $host,'port'=>$port]))->addSelect(['session_actives','session_waits','sqlmonitor_count','temp_tbs_sql_count','create_time'])->andFilterWhere(['>=', 'YmdHi', $start_time])->andFilterWhere(['<=', 'YmdHi', $end_time])->all();
        return $this->render('view', [
            'model' => $this->findModel($server),
            'chart_data' => $chart_data,
        ]);
    }

    /**
     * Creates a new OracleStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OracleStatus();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OracleStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OracleStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MysqlStatus model based on its host and port value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param char $server
     * @return OracleStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($server)
    {
        $server_array = explode('-',$server);
        $host = $server_array[0];
        $port = $server_array[1];
        if (($model = OracleStatus::find()->where(['host' => $host,'port'=>$port])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    function actionChart(){
        if($post = Yii::$app->request->post()){
            $host = $post['OracleStatus']['host'];
            $port = $post['OracleStatus']['port'];
            $time_from = $post['time_from'];
            $time_to = $post['time_to'];
            $start_time = date('YmdHi',strtotime($time_from));
            $end_time = date('YmdHi',strtotime($time_to));
            $time_interval = 0;

        }
        else{
            $server = Yii::$app->getRequest()->get()['server'];
            $server_array = explode('-',$server);
            $host = $server_array[0];
            $port = $server_array[1];

            $time_interval = Yii::$app->getRequest()->get()['time_interval'];
            $end = Yii::$app->getRequest()->get()['end'];
            $start_time = date('YmdHi',(time()-$end-$time_interval));
            $end_time = date('YmdHi',(time()-$end));
            $time_from = date('Y-m-d',time()-3600*24);
            $time_to = date('Y-m-d',time());
        }

        if (($model = OracleStatus::findOne(['host' => $host,'port'=>$port])) !== null) {
            //$chart_data = OracleStatusHistory::find()->join('left join','oracle_server','oracle_status_history.host=oracle_server.host and oracle_status_history.port=oracle_server.port')->where((['oracle_status_history.host' => $host,'oracle_status_history.port'=>$port]))->andFilterWhere(['>=', 'oracle_status_history.YmdHi', $start_time])->andFilterWhere(['<=', 'oracle_status_history.YmdHi', $end_time])->all();
            $chart_data = OracleStatusHistory::find()->joinWith('servers')->where((['oracle_status_history.host' => $host,'oracle_status_history.port'=>$port]))->andFilterWhere(['>=', 'YmdHi', $start_time])->andFilterWhere(['<=', 'YmdHi', $end_time])->all();
            $os_chart_data = OsStatusHistory::find()->joinWith('servers')->where((['os_status_history.ip' => $host]))->andFilterWhere(['>=', 'YmdHi', $start_time])->andFilterWhere(['<=', 'YmdHi', $end_time])->all();

            return $this->render('chart', [
                'chart_data' => $chart_data,
                'os_chart_data' => $os_chart_data,
                'model' => $model,
                'time_interval' => $time_interval,
                'time_from'=>$time_from,
                'time_to'=>$time_to,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

    public function actionTablespace($server)
    {
        $server_array = explode('-',$server);
        $host = $server_array[0];
        $port = $server_array[1];

        $searchModel = new OracleTablespaceSearch();

        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($params);
        $dataProvider->query->andWhere(['host'=>$host,'port'=>$port]);
        return $this->render('tablespace', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($server),
        ]);
    }

    public function actionObjects($server)
    {
        $server_array = explode('-',$server);
        $host = $server_array[0];
        $port = $server_array[1];

        $searchModel = new OracleObjectsSearch();

        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($params);
        $dataProvider->query->andWhere(['host'=>$host,'port'=>$port]);
        return $this->render('objects', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($server),
        ]);
    }

    public function actionIndexes($server)
    {
        $server_array = explode('-',$server);
        $host = $server_array[0];
        $port = $server_array[1];

        $searchModel = new OracleIndexesSearch();

        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($params);
        $dataProvider->query->andWhere(['host'=>$host,'port'=>$port]);
        return $this->render('indexes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($server),
        ]);
    }

    public function actionAlarm($server)
    {
        $server_array = explode('-',$server);
        $host = $server_array[0];
        $port = $server_array[1];

        $params = Yii::$app->request->queryParams;
        $searchModel = new AlarmSearch();
        $dataProvider = $searchModel->search($params);
        $dataProvider->query->andWhere(['host' => "$host", 'port' => "$port" ]);

        return $this->render('alarm', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($server),
        ]);
    }

    public function actionAlarmView($id)
    {
        if (($model = Alarm::findOne($id)) !== null) {
            return $this->render('alarm_view', [
                'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

    public function actionWaits($server)
    {
        $server_array = explode('-',$server);
        $host = $server_array[0];
        $port = $server_array[1];

        $searchModel = new OracleWaitsSearch();

        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($params);
        $dataProvider->query->andWhere(['host'=>$host,'port'=>$port]);
        return $this->render('waits', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($server),
        ]);
    }
}
