<?php

namespace backend\modules\os\models;

use Yii;

/**
 * This is the model class for table "os_net".
 *
 * @property integer $id
 * @property string $ip
 * @property string $tags
 * @property string $if_descr
 * @property string $in_bytes
 * @property string $out_bytes
 * @property string $create_time
 */
class OsNet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'os_net';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip'], 'required'],
            [['in_bytes', 'out_bytes'], 'integer'],
            [['create_time'], 'safe'],
            [['ip', 'if_descr'], 'string', 'max' => 50],
            [['tags'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'tags' => '标签',
            'if_descr' => '网卡描述符',
            'in_bytes' => '入流量',
            'out_bytes' => '出流量',
            'create_time' => 'Create Time',
        ];
    }
}
