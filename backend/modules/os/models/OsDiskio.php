<?php

namespace backend\modules\os\models;

use Yii;

/**
 * This is the model class for table "os_diskio".
 *
 * @property integer $id
 * @property string $ip
 * @property string $tags
 * @property string $fdisk
 * @property string $disk_io_reads
 * @property string $disk_io_writes
 * @property string $create_time
 */
class OsDiskio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'os_diskio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip'], 'required'],
            [['disk_io_reads', 'disk_io_writes'], 'integer'],
            [['create_time'], 'safe'],
            [['ip', 'fdisk'], 'string', 'max' => 50],
            [['tags'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'tags' => 'Tags',
            'fdisk' => 'Fdisk',
            'disk_io_reads' => 'Disk Io Reads',
            'disk_io_writes' => 'Disk Io Writes',
            'create_time' => 'Create Time',
        ];
    }
}
