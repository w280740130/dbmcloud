<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\hbase\models\HbaseRsStatus */

$this->title = $model->host . ':' . $model->port;
$this->params['breadcrumbs'][] = ['label' => 'Hbase Region Server 性能监控', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host . ':' . $model->port;
?>

<?php echo $this->render('_tab', ['model' => $model]); ?>

<div class="container-fluid mt5">
    <div class="col-xs-8 text-left">
        <table class="table table-bordered table-hover table-striped" style="margin-top: 15px;">
            <thead>
            <tr>
                <th>指标</th>
                <th>数值</th>
                <th>备注</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>实例名称</td>
                <td><?= $model->host . ':' . $model->port ?></td>
                <td>当前数据库的连接主机和端口信息</td>
            </tr>
            <tr>
                <td>采集时间</td>
                <td><?= $model->createtime ?></td>
                <td>采集进程最后执行采集任务的时间</td>
            </tr>
            <tr>
                <td>实例状态</td>
                <td><span class="btn btn-success btn-xs">运行</span></td>
                <td>当前实例是否正常连接</td>
            </tr>

            <tr>
                <td>readRequestCount</td>
                <td><?= $model->readRequestCount_sub; ?></td>
                <td>每秒读请求,该值最好低于5000</td>
            </tr>
            <tr>
                <td>writeRequestCount</td>
                <td><?= $model->writeRequestCount_sub; ?></td>
                <td>每秒写请求,该值最好低于5000</td>
            </tr>
            <tr>
                <td>updatesBlockedTime</td>
                <td><?= $model->updatesBlockedTime; ?></td>
                <td>当请求被lock的时间,不要超过10，越低越好</td>
            </tr>
            <tr>
                <td>queueSize</td>
                <td><?= $model->queueSize; ?></td>
                <td>queue的大小,这个反应请求是否及时处理或者是请求量过大</td>
            </tr>
            <tr>
                <td>QueueCallTime</td>
                <td><?= $model->QueueCallTime_num_ops_sub; ?></td>
                <td>每秒queuecall处理的次数</td>
            </tr>
            <tr>
                <td>GcCount</td>
                <td><?= $model->GcCount; ?></td>
                <td>gc总共次数,自启动累加</td>
            </tr>
            <tr>
                <td>GcTimeMillis</td>
                <td><?= $model->GcTimeMillis; ?></td>
                <td>gc总共时间,自启动累加</td>
            </tr>
            <tr>
                <td>flushQueueLength</td>
                <td><?= $model->flushQueueLength; ?></td>
                <td>flush队列长度,最好等于0,如果很大，说明性能有问题，估计是并发很高</td>
            </tr>
            <tr>
                <td>memStoreSize</td>
                <td><?= $model->memStoreSize; ?></td>
                <td>allMemstore的内存大小，写入数据在内存的大小</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="col-xs-4 text-center ">
        <div id="region-count" class="line-chart-x4 mt5"></div>
        <div id="main_queueSize" class="line-chart-x4 mt5"></div>
    </div>
</div>


<?php \common\widgets\JsBlock::begin() ?>
<script type="text/javascript">

    var myChart = echarts.init(document.getElementById('region-count'));

    myChart.setOption({
        //backgroundColor: '#FFF',
        title : {
            text: '',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder',
                color: '#FFFFFF'
            }
        },
        legend: {
            data:['Region Count','Store Count','File Count'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '40px',
            x2: '30px',
            y: '30px',
            y2: '30px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : false,
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Region Count',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['regionCount'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Store Count',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['storeCount'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

            {
                name:'File Count',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['storeFileCount'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
        ]
    });

</script>

<script type="text/javascript">

    var myChart = echarts.init(document.getElementById('main_queueSize'));

    myChart.setOption({
        //backgroundColor: '#FFF',
        title : {
            text: '',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder',
                color: '#FFFFFF'
            }
        },
        legend: {
            data:['Queue Size'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '40px',
            x2: '30px',
            y: '30px',
            y2: '30px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : false,
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Queue Size',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['queueSize'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>


<?php \common\widgets\JsBlock::end()?>
