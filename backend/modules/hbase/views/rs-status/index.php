<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\hbase\models\HbaseRsStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hbase Region Server 性能监控';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hbase-rs-status-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'host',
            'port',
            'tags',
            [
                'attribute' => 'connect',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->connect == 1 ? "<span class='btn btn-xs bg-green btn-flat'>正常</span>" : "<span class='btn btn-xs bg-red btn-flat'>异常</span>";
                    },

                'headerOptions' => ['width' => '75'],

            ],
             //'readRequestCount_sub',
             //'writeRequestCount_sub',
             //'compactionQueueLength',
             //'flushQueueLength',
             'regionCount',
             'storeCount',
             'storeFileCount',
             //'totalRequestCount_sub',
            // 'updatesBlockedTime:datetime',
             'totalRequestCount',
             'GcCount',
            // 'GcTimeMillis:datetime',
            // 'GcTimeMillisConcurrentMarkSweep:datetime',
            // 'GcCountConcurrentMarkSweep',
            // 'queueSize',
            // 'QueueCallTime_num_ops_sub:datetime',
            // 'numCallsInGeneralQueue',
            // 'memStoreSize',
            // 'blockCacheSize',
            // 'MemHeapUsedM',
            // 'createtime',
            // 'readRequestCount',
            // 'writeRequestCount',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Operate'),
                'headerOptions' => ['width' => '75'],
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',  Yii::$app->urlManager->createUrl(['hbase/rs-status/view','server'=>$model->host.'--'.$model->port]), ['title' => '查看监控详情' ]);
                    },

                ],

            ],
        ],
    ]); ?>

</div>
