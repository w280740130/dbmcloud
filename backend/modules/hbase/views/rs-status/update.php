<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\hbase\models\HbaseRsStatus */

$this->title = 'Update Hbase Rs Status: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hbase Rs Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hbase-rs-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
