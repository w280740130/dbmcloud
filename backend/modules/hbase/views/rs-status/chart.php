<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\widgets\DetailView;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatus */

$this->title = $model->host . ':' . $model->port;
$this->params['breadcrumbs'][] = ['label' => 'Hbase Region Server 性能监控', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host . ':' . $model->port;
?>

<?php
$data = $model->find()->orderBy(['id' => SORT_DESC, 'host' => SORT_ASC])->all();
echo $this->render('_tab',['model'=>$model]);
?>

<div class="container-fluid mt15">
    <div class="col-xs-5 text-left">
        <a href="<?= Yii::$app->urlManager->createUrl(['hbase/rs-status/chart/','server'=>$model->host.'--'.$model->port,'time_interval'=>'3600','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==3600): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">1小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['hbase/rs-status/chart/','server'=>$model->host.'--'.$model->port,'time_interval'=>'21600','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==21600): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">6小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['hbase/rs-status/chart/','server'=>$model->host.'--'.$model->port,'time_interval'=>'43200','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==43200): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">12小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['hbase/rs-status/chart/','server'=>$model->host.'--'.$model->port,'time_interval'=>'86400','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==86400): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">1天</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['hbase/rs-status/chart/','server'=>$model->host.'--'.$model->port,'time_interval'=>'259200','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==259200): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">3天</a>
    </div>
    <?php $form = ActiveForm::begin(['action'=>['rs-status/chart'],'method'=>'post']); ?>
    <div class="col-xs-5 text-left">

        <?= DateRangePicker::widget([
            'name' => 'time_from',
            'value' => $time_from,
            'nameTo' => 'time_to',
            'valueTo' => $time_to,
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-m-d'
            ]
        ]);?>

    </div>
    <div class="col-xs-1 text-left">
        <?= Html::activeHiddenInput($model,'host'); ?>
        <?= Html::activeHiddenInput($model,'port'); ?>
        <?= Html::submitButton('生成图表', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<hr/>
<div class="container-fluid mt15">

    <a href="#main_chart_connect" class="btn btn-warning">连接状态</a>
    <a href="#main_chart_region_request" class="btn btn-warning">Region Request</a>
    <a href="#main_chart_queue_length" class="btn btn-warning">Queue Length</a>
    <a href="#main_chart_region_count" class="btn btn-warning">Region Count</a>
    <a href="#main_chart_blocked_time" class="btn btn-warning">Blocked Time</a>
    <a href="#main_chart_gc_count" class="btn btn-warning">GC Count</a>
    <a href="#main_chart_queue_call_time" class="btn btn-warning">Queue CallTime</a>

    <hr/>

    <div id="main_chart_connect" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_region_request" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_queue_length" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_region_count" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_blocked_time" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_gc_count" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_queue_call_time" class="col-xs-12 text-center line-chart"></div>
</div>



<?php \common\widgets\JsBlock::begin() ?>

<script type="text/javascript">
    //连接状态
    var myChart = echarts.init(document.getElementById('main_chart_connect'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933','#FF3333'],
        title : {
            text: '连接状态',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['连接状态'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'连接状态',
                type:'line',
                smooth:true,
                itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['connect'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //Region Request Count
    var myChart = echarts.init(document.getElementById('main_chart_region_request'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933','#FF3333'],
        title : {
            text: 'Region Request Count',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Read Count','Write Count'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Read Count',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['readRequestCount_sub'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Write Count',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['writeRequestCount_sub'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>


<script type="text/javascript">
    //Queue Length
    var myChart = echarts.init(document.getElementById('main_chart_queue_length'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#FF9933','#66CC66'],
        title : {
            text: 'Queue Length',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Compaction','Flush'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Compaction',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['compactionQueueLength'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Flush',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['flushQueueLength'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //Region Count
    var myChart = echarts.init(document.getElementById('main_chart_region_count'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        //color:['#666699'],
        title : {
            text: 'Region Count',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Region Count','Store Count','Store File Count'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Region Count',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['regionCount'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Store Count',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['storeCount'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Store File Count',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['storeFileCount'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>



<script type="text/javascript">
    //updatesBlockedTime
    var myChart = echarts.init(document.getElementById('main_chart_blocked_time'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#990033'],
        title : {
            text: 'Blocked Time',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['updatesBlockedTime'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'updatesBlockedTime',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['updatesBlockedTime'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //GC Count
    var myChart = echarts.init(document.getElementById('main_chart_gc_count'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#66CC99'],
        title : {
            text: 'GC Count',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['GC Count'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'GC Count',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['GcCount'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //Queue Call Time
    var myChart = echarts.init(document.getElementById('main_chart_queue_call_time'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#66CCFF'],
        title : {
            text: 'Queue Call Time',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['Queue Call Time Num OPS'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'Queue Call Time Num OPS',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['QueueCallTime_num_ops_sub'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<?php \common\widgets\JsBlock::end()?>


