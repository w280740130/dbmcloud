<?php

namespace backend\modules\hbase\controllers;

use backend\modules\hbase\models\HbaseRsStatusHistory;
use Yii;
use backend\modules\hbase\models\HbaseRsStatus;
use backend\modules\hbase\models\HbaseRsStatusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RsStatusController implements the CRUD actions for HbaseRsStatus model.
 */
class RsStatusController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all HbaseRsStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HbaseRsStatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HbaseRsStatus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($server)
    {
        $server_array = explode('--',$server);
        $host = $server_array[0];
        $port = $server_array[1];

        $time_interval = 1800;
        $end = 0;
        $start_time = date('YmdHi',(time()-$end-$time_interval));
        $end_time = date('YmdHi',(time()-$end));
        $chart_data = HbaseRsStatusHistory::find()->where((['host' => $host,'port'=>$port]))->addSelect(['create_time','queueSize','regionCount','storeCount','storeFileCount','QueueCallTime_num_ops_sub','updatesBlockedTime','readRequestCount_sub','writeRequestCount_sub','GcCount'])->andFilterWhere(['>=', 'YmdHi', $start_time])->andFilterWhere(['<=', 'YmdHi', $end_time])->all();
        return $this->render('view', [
            'model' => $this->findModel($server),
            'chart_data' => $chart_data,
        ]);
    }

    /**
     * Creates a new HbaseRsStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HbaseRsStatus();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing HbaseRsStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing HbaseRsStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HbaseRsStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HbaseRsStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($server)
    {
        $server_array = explode('--',$server);
        $host = $server_array[0];
        $port = $server_array[1];
        if (($model = HbaseRsStatus::find()->where(['host' => $host,'port'=>$port])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    function actionChart(){
        if($post = Yii::$app->request->post()){
            $host = $post['HbaseRsStatus']['host'];
            $port = $post['HbaseRsStatus']['port'];
            $time_from = $post['time_from'];
            $time_to = $post['time_to'];
            $start_time = date('YmdHi',strtotime($time_from));
            $end_time = date('YmdHi',strtotime($time_to));
            $time_interval = 0;

        }
        else{
            $server = Yii::$app->getRequest()->get()['server'];
            $server_array = explode('--',$server);
            $host = $server_array[0];
            $port = $server_array[1];

            $time_interval = Yii::$app->getRequest()->get()['time_interval'];
            $end = Yii::$app->getRequest()->get()['end'];
            $start_time = date('YmdHi',(time()-$end-$time_interval));
            $end_time = date('YmdHi',(time()-$end));
            $time_from = date('Y-m-d',time()-3600*24);
            $time_to = date('Y-m-d',time());
        }



        //echo $start_time;exit;
        if (($model = HbaseRsStatus::findOne(['host' => $host,'port'=>$port])) !== null) {
            $chart_data = HbaseRsStatusHistory::find()->where((['host' => $host,'port'=>$port]))->andFilterWhere(['>=', 'YmdHi', $start_time])->andFilterWhere(['<=', 'YmdHi', $end_time])->all();
            return $this->render('chart', [
                'chart_data' => $chart_data,
                'model' => $model,
                'time_interval' => $time_interval,
                'time_from'=>$time_from,
                'time_to'=>$time_to,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }
}
