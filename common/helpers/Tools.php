<?php
/**
 * Created by PhpStorm.
 * User: ruzuojun
 * Date: 2015/10/15
 * Time: 12:42
 */

namespace Common\helpers;

class Tools
{


    static function ConvertNumber($data)
    {
        if ($data == 0) {
            $result = "顶";
        } elseif ($data == 1) {
            $result = "一";
        } elseif ($data == 2) {
            $result = "二";
        } elseif ($data == 3) {
            $result = "三";
        } elseif ($data == 4) {
            $result = "四";
        } elseif ($data == 5) {
            $result = "五";
        } elseif ($data == 6) {
            $result = "六";
        } elseif ($data == 7) {
            $result = "七";
        } elseif ($data == 8) {
            $result = "八";
        } elseif ($data == 9) {
            $result = "九";
        } else {
            $result = '顶';
        }
        return $result;
    }

    static function FormatTime($data)
    {

        if ($data == -1) {
            return "---";
        } else if ($data < 60) {
            $result = $data . " 秒";
            return $result;
        } else if ($data >= 60 and $data < 3600) {
            $result = number_format(($data / 60)) . " 分钟";
            return $result;
        } else if ($data >= 3600 and $data < 86400) {
            $result = number_format(($data / 3600)) . " 小时";
            return $result;
        } else if ($data >= 86400) {
            $result = number_format(($data / 86400), 0) . " 天";
            return $result;
        }

    }

    static function formatBytes($data)
    {
        if ($data == -1) {
            return '---';
        } else if ($data >= 0 and $data < 1024) {
            return number_format(($data)) . "B";
        } else if ($data >= 1024 and $data < 1048576) {
            return number_format(($data / 1024)) . "KB";
        } else if ($data >= 1048576 and $data < 1073741824) {
            return number_format(($data / 1024 / 1024)) . "MB";
        } else {
            return number_format(($data / 1024 / 1024 / 1024)) . "GB";
        }
    }

    static function formatKbytes($data)
    {
        if ($data == -1) {
            return '---';
        } else if ($data >= 0 and $data < 1024) {
            return number_format(($data)) . "KB";
        } else if ($data >= 1024 and $data < 1048576) {
            return number_format(($data / 1024)) . "MB";
        } else if ($data >= 1048576 and $data < 1073741824) {
            return number_format(($data / 1024 / 1024), 1) . "GB";
        } else {
            return number_format(($data / 1024 / 1024 / 1024), 1) . "TB";
        }
    }

    static function formatMbytes($data)
    {
        if ($data == -1) {
            return '---';
        } else if ($data > 0 and $data < 1024) {
            return number_format(($data)) . "MB";
        } else if ($data >= 1024 and $data < 1048576) {
            return number_format(($data / 1024), 1) . "GB";
        } else {
            return number_format(($data / 1024 / 1024), 1) . "TB";
        }
    }

    // Xml 转 数组, 包括根键
    function xml_to_array($xml)
    {
        $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
        if (preg_match_all($reg, $xml, $matches)) {
            $count = count($matches[0]);
            for ($i = 0; $i < $count; $i++) {
                $subxml = $matches[2][$i];
                $key = $matches[1][$i];
                if (preg_match($reg, $subxml)) {
                    $arr[$key] = xml_to_array($subxml);
                } else {
                    $arr[$key] = $subxml;
                }
            }
        }
        return $arr;
    }

    // Xml 转 数组, 不包括根键
    function xmltoarray($xml)
    {
        $arr = xml_to_array($xml);
        $key = array_keys($arr);
        return $arr[$key[0]];
    }


}