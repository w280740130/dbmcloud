<?php

/*
//跨域session域名配置,获取当前主机名
//$_SERVER["HTTP_HOST"] = 'www.yihuike.com';
$host_array = explode('.', $_SERVER["HTTP_HOST"]);
//针对com域名，获取顶级域名
if (count($host_array) == 3) {
    define('DOMAIN', $host_array[1] . '.' . $host_array[2]);
}
else{
    echo "本系统不支持本地访问，请配置域名";exit;
}
define('DOMAIN_HOME', 'www.' . DOMAIN);
define('DOMAIN_CONSOLE', 'console.' . DOMAIN);
define('DOMAIN_STORAGE', 'storage.' . DOMAIN);
define('DOMAIN_API', 'api.' . DOMAIN);
*/

return [
    'name' => 'Lepus Plus',
    'language' => 'zh-CN',
    'timeZone' => 'Asia/Shanghai',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'attachments' => [
            'class' => nemmo\attachments\Module::className(),
            'tempPath' => '@storage/attachments/temp',
            'storePath' => '@storage/attachments/store',
            'rules' => [ // Rules according to the FileValidator
                    'maxFiles' => 10, // Allow to upload maximum 3 files, default to 3
                    'mimeTypes' => ['image/png','image/jpeg','image/gif','application/msword','application/msexcel','application/mspowerpoint','application/pdf','text/html'], // Only png images
                    'maxSize' => 1024 * 1024 * 5 // 5 MB
            ],
        'tableName' => '{{%attachments}}' // Optional, default to 'attach_file'
        ]
    ],
    'components' => [
        'user' => [
            //'class' => '',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            //'identityCookie' => ['name' => '_identity', 'httpOnly' => true, 'domain' => '.' . DOMAIN],
        ],
        'session' => [
            //'class' => '',
            //'cookieParams' => ['domain' => '.' . DOMAIN, 'lifetime' => 0],
            'timeout' => 3600,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            //'defaultRoles' => ['guest'],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        /*
        'urlManager' => [
            'class' => 'common\components\MutilpleDomainUrlManager',
            'domains' => [
                'www' => '//' . DOMAIN_HOME,
                'console' => '//' . DOMAIN_CONSOLE,
                'storage' => '//' . DOMAIN_STORAGE,
                'api' => '//' . DOMAIN_API,
            ],
            'showScriptName' => false,
            'enablePrettyUrl' => true,
        ],
        */
        'smser' => [
            // 海岩短信发送通道
            'class' => 'daixianceng\smser\SmshySmser',
            'userid' => '12978',
            'account' => 'nrens01',
            'password' => '',
            'useFileTransport' => false
        ],
        'uuid' => [
            'class' => 'ollieday\uuid\Uuid',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                /*'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'zh-CN',
                    'basePath' => '@app/messages'
                ],*/
            ],
        ],
        'formatter' => [
            'dateFormat' => 'yyyy-MM-dd',
            'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'CNY',
        ],
		
    ],
    'aliases' => [
        '@funson86/cms' => '@vendor/funson86/cms',
        '@funson86/blog' => '@vendor/funson86/blog',
        '@kartik/export' => '@vendor/kartik-v/yii2-export',
        '@kartik/grid' => '@vendor/kartik-v/yii2-grid',
        '@kartik/base' => '@vendor/kartik-v/yii2-krajee-base',
        '@kartik/mpdf' => '@vendor/kartik-v/yii2-mpdf',
        '@c006/alerts' => '@vendor/c006/yii2-alerts',
        '@storage_root' => $_SERVER['DOCUMENT_ROOT'].'/yiicenter/storage',
    ],
];
