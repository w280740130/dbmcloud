<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\web\IdentityInterface;


class Base extends \yii\db\ActiveRecord
{

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_HIDDEN = 0;
    const STATUS_DISPLAY = 1;
    const STATUS_DELETED = -1;

    const STATUS_YES = 1;
    const STATUS_NO = 0;

    const STATUS_ON = 1;
    const STATUS_OFF = 0;

    const STATUS_ON_SHELF = 1;
    const STATUS_OFF_SHELF = 0;


    static public function getActiveStatus()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'STATUS_ACTIVE'),
            self::STATUS_INACTIVE => Yii::t('app', 'STATUS_INACTIVE'),
        ];
    }

    static public function getYnStatus()
    {
        return [
            self::STATUS_YES => Yii::t('app', 'YES'),
            self::STATUS_NO => Yii::t('app', 'NO'),
        ];
    }

    static public function getOnStatus()
    {
        return [
            self::STATUS_ON => Yii::t('app', 'ON'),
            self::STATUS_OFF => Yii::t('app', 'OFF'),
        ];
    }

    static public function getShelfStatus()
    {
        return [
            self::STATUS_ON_SHELF => Yii::t('app', 'STATUS_ON_SHELF'),
            self::STATUS_OFF_SHELF => Yii::t('app', 'STATUS_OFF_SHELF'),
        ];
    }

    static public function getDefaultStatus()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Yes'),
            self::STATUS_INACTIVE => Yii::t('app', 'No'),
        ];
    }

    static public function getDisplayStatus()
    {
        return [
            self::STATUS_DISPLAY => Yii::t('app', 'STATUS_DISPLAY'),
            self::STATUS_HIDDEN => Yii::t('app', 'STATUS_HIDDEN'),
        ];
    }

    public static  function getTree($pid = 0, $array = array(), $level = 0, $add = 1, $repeat = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') {

        $str_repeat = '';
        if ($level) {
            for($j = 0; $j < $level; $j ++) {
                $str_repeat .= $repeat;
            }
            $str_repeat.= '&darr;&rarr;';
        }
        $newarray = array ();
        $temparray = array ();

        foreach ( ( array ) $array as $v ) {
            if ($v['parent_id'] == $pid) {
                $v['level'] = $level;
                $v['str_repeat'] = $str_repeat;

                $newarray[] = $v;

                $temparray = self::getTree( $v ['id'], $array, ($level + $add) );
                if ($temparray) {
                    $newarray = array_merge ( $newarray, $temparray );
                }
            }
        }
        return $newarray;
    }


}
